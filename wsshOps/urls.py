"""wsshOps URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from  apps.rbac import views
from apps.rbac import extra_views
from  apps.webssh.views import disconncet_wssh
from django.urls import path,include


urlpatterns = [
    path("",views.IndexView.as_view()),
    path("login/", extra_views.LoginView.as_view()),
    path("logout/", extra_views.Logout),
    path("rbac/", include("apps.rbac.urls")),
    path("cmdb/", include("apps.cmdb.urls")),
    path("log/", include("apps.logs.urls")),
    path("ws/<int:host_id>/", include("apps.webssh.urls")),
    path('disconnect/', disconncet_wssh),
]
