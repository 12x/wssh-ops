# xiao2r

#### 介绍
项目由python 3.6 基于django 3.0开发，由cmdb,rabc,log三大功能模块组成，其中通过websocket+channel实现了服务器的webssh功能，并通过asciinema 实现终端操作回放，强制下线等


#### 软件架构




#### 安装教程

1. 安装mysql 5.7

```
wget -i -c http://dev.mysql.com/get/mysql57-community-release-el7-10.noarch.rpm

yum -y install mysql57-community-release-el7-10.noarch.rpm

yum -y install mysql-community-server

systemctl start  mysqld.service
systemctl enable mysqld.service

# 修改密码
mysqladmin -uroot -p password new_passwd

# 创建数据库 
create database wsshOps

# 导入数据
mysql -uroot -p wsshOps < wsshOps.sql

```


2. 安装 redis

```
yum install -y redis
```


3. 安装 python 3.6

```
yum -y install gcc-c++ openssl-devel

wget https://www.python.org/ftp/python/3.7.0/Python-3.7.0.tar.xz

#解压安装
mkdir /usr/local/python37
tar -xf Python-3.7.0.tar.xz 
cd Python-3.7.0
./configure --enable-optimizations --prefix=/usr/local/python37/ --with-ssl
make && make install

```


4. 安装依赖
`pip install -r requirements.txt`




#### 项目展示

1. 首页
![首页](https://images.gitee.com/uploads/images/2020/1012/161930_ea8cc7d7_578265.png "屏幕截图.png")
2. 资产管理
![资产管理](https://images.gitee.com/uploads/images/2020/1012/162003_4f097fc4_578265.png "屏幕截图.png")
3. webssh
![webssh](https://images.gitee.com/uploads/images/2020/1012/162039_904978bc_578265.png "屏幕截图.png")
4. 终端回放
![终端回放](https://images.gitee.com/uploads/images/2020/1012/162112_e6e76c69_578265.png "屏幕截图.png")
5. 角色管理
![角色管理](https://images.gitee.com/uploads/images/2020/1012/162145_2d567c21_578265.png "屏幕截图.png")