from openpyxl import load_workbook


def import_host(filename):
    """读取导入的excel资产信息"""
    wb = load_workbook(filename=filename, read_only=True)
    ws = wb.active
    data = []
    N = 1
    for row in ws.rows:
        if N == 1:
            N += 1
        else:
            n = 1
            row_info = []
            for cell in row:
                row_info.append(cell.value)
                n += 1
                if n == 10:
                    data.append(row_info)
                else:
                    continue
            N += 1
    return data


if __name__ == "__main__":
    data = import_host('/tmp/pycharm_project_108/statics/medias/template/asset_template.xlsx')
    print(data)
