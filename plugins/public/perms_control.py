from django.db.models import Q
from apps.rbac import models as rbac_db


def menus_list(username):
    '''获取菜单列表'''
    user_obj = rbac_db.User.objects.get(username=username)
    data_list = []
    if user_obj.is_superuser == "1":
        one_menu = rbac_db.Permission.objects.filter(perms_type__contains="一级菜单").order_by("weight")
        for i in one_menu:
            two_menu = rbac_db.Permission.objects.filter(parent_id=i.id).order_by("id")
            two_menu_list = []
            for j in two_menu:
                two_menu_list.append({"id": j.id, 'perms': j.title, 'perms_url': j.url, 'perms_img': j.perms_icon})
            data_list.append({"id": i.id, 'perms': i.title, 'perms_url': i.url, 'perms_img': i.perms_icon,
                              "two_menu": two_menu_list})
    else:
        role_obj = rbac_db.Role.objects.get(id=user_obj.role_id)
        perms_obj = role_obj.permissions.all().order_by("weight", "id")
        one_menu_list = []
        for i in perms_obj:
            perms_type = i.perms_type
            if perms_type == u'一级菜单':
                one_menu_list.append(
                    {"id": i.id, 'perms': i.title, 'perms_url': i.url, 'perms_img': i.perms_icon, "perms_id": i.id})
        for i in one_menu_list:
            two_menu_list = []
            for j in perms_obj:
                if j.parent_id == i["perms_id"]:
                    two_menu_list.append({"id": j.id, 'perms': j.title, 'perms_url': j.url, 'perms_img': j.perms_icon})
            i["two_menu"] = two_menu_list
            data_list.append(i)
    return data_list


def get_perms_list(role_id):
    '''获取菜单列表'''
    role_obj = rbac_db.Role.objects.get(id=role_id)
    perms_obj = role_obj.permissions.all().order_by("weight", "id")
    one_menu_list = []
    data_list = []
    for i in perms_obj:
        perms_type = i.perms_type
        if perms_type == u'一级菜单':
            one_menu_list.append(
                {"id": i.id, 'perms': i.title, 'perms_url': i.url, 'perms_img': i.perms_icon, "perms_id": i.id})

    for i in one_menu_list:
        two_menu_list = []
        for j in perms_obj:
            if j.parent_id == i["perms_id"]:
                two_menu_list.append({"id": j.id, 'perms': j.title, 'perms_url': j.url, 'perms_img': j.perms_icon})
        i["two_menu"] = two_menu_list
        data_list.append(i)
    return data_list


def perms_list(username):
    user_obj = rbac_db.User.objects.get(username=username)
    role_id = user_obj.role_id
    role_obj = rbac_db.Role.objects.get(id=role_id)
    perms_all_list = []
    if user_obj.is_superuser == "1":
        perms_obj = rbac_db.Permission.objects.all()
        for i in perms_obj:
            perms_all_list.append(i.perms_id)
    else:
        perms_obj = role_obj.permissions.all()
        for i in perms_obj:
            perms_all_list.append(i.perms_id)
    return perms_all_list


def perms_id_list(role_id):
    role_obj = rbac_db.Role.objects.get(id=role_id)
    perms_all_list = []
    perms_obj = role_obj.permissions.all()
    for i in perms_obj:
        perms_all_list.append(i.id)
    return perms_all_list
