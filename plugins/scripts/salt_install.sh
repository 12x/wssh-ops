#!/bin/bash

CENTOS_VER=`rpm -q centos-release|cut -d- -f3`

case "${CENTOS_VER}" in
    "7")
    #关闭selinux
    sed -i "s/^SELINUX=.*/SELINUX=disabled/" /etc/selinux/config
    setenforce 0

    yum -y install https://repo.saltstack.com/yum/redhat/salt-repo-latest-2.el7.noarch.rpm >&/dev/null
    
    LOCAL_IP=`/sbin/ifconfig|grep "inet" |grep -v "127.0.0.1"|head -n 1|awk '{print $2}'`
    ;;
    "6")
    #关闭防火墙

    #关闭selinux
    sed -i "s/^SELINUX=.*/SELINUX=disabled/" /etc/selinux/config
    setenforce 0

    yum -y install https://repo.saltstack.com/yum/redhat/salt-repo-latest-2.el6.noarch.rpm >&/dev/null
    
    #配置mysql yum 源
    
    LOCAL_IP=`/sbin/ifconfig|grep "inet addr" |grep -v "127.0.0.1"|head -n 1|cut -d: -f2|awk '{print $1}'`
    ;;
esac

yum -y install salt-minion  >&/dev/null
cat >/etc/salt/minion<<EOF
master: 192.168.145.128
id: $LOCAL_IP
EOF

systemctl enable salt-minion || chkconfig salt-minion on
systemctl start salt-minion || service salt-minion start

