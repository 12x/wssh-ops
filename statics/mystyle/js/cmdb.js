///////////////////////分组/////////////////////////////
//添加分组

$("#sub-group").click(function () {
    var group_name = $("#group-name").val();
    var group_msg = $("#group-msg").val();
    $.ajax({
        url: "/cmdb/group/",
        type: 'POST',
        data: {
            'group_name': group_name,
            'group_msg': group_msg,
        },
        success: function (data) {
            var ret = eval('(' + data + ')');
            if (ret.status == "False") {
                spop({
                    template: ret.info,
                    style: 'warning',
                    autoclose: 5000
                });
            } else {
                $("#groupModal").modal("hide");
                spop({
                    template: ret.info,
                    style: 'success',
                    autoclose: 2000
                });
                setTimeout("location.reload()", 2000);
            }
        }
    })
});

//获取编辑group信息
$('td a[name="edit-group"]').click(function () {
    var group_id = $(this).attr("group_id");
    $.ajax({
        url: "/cmdb/group/",
        type: "PUT",
        data: JSON.stringify({'group_id': group_id}),
        success: function (data) {
            var ret = eval('(' + data + ')');
            if (ret.status == "False") {
                spop({
                    template: ret.info,
                    style: 'warning',
                    autoclose: 5000
                });
            } else {
                if (ret.status == "True") {
                    info = ret.info;
                    $("#edit-group-name").val(info.group_name);
                    $("#edit-group-msg").val(info.group_msg);
                    $("#edit-contact").val(info.user_id);
                    $("#sub-edit-group").attr('group_id', info.group_id);
                    $("#edit-groupModal").modal('show');
                } else {
                    spop({
                        template: ret.info,
                        autoclose: 2000
                    });
                }

            }

        }
    });
});

//修改group信息
$("#sub-edit-group").click(function () {
    var group_id = $(this).attr("group_id");
    var group_name = $("#edit-group-name").val();
    var group_msg = $("#edit-group-msg").val();
    var user_id = $("#edit-contact").val();
    $.ajax({
        url: "/cmdb/group/",
        type: "PUT",
        data: JSON.stringify({
            'action': 'edit', 'group_id': group_id, 'group_name': group_name,
            'group_msg': group_msg, 'user_id': user_id
        }),
        success: function (data) {
            var ret = eval('(' + data + ')');
            if (ret.status == "False") {
                spop({
                    template: ret.info,
                    style: 'warning',
                    autoclose: 5000
                });
            } else {
                $("#edit-groupModal").modal("hide");
                spop({
                    template: ret.info,
                    style: 'success',
                    autoclose: 2000
                });
                setTimeout("location.reload()", 2000);
            }
        }
    });
});

//删除group
$("td a[name='del-group']").click(function () {
    var group_id = $(this).attr('group_id');
    var statu = confirm("是否确认删除！");
    if (statu == true) {
        $.ajax({
            url: "/cmdb/group/",
            type: "DELETE",
            data: JSON.stringify({'group_id': group_id}),
            success: function (data) {
                var ret = eval('(' + data + ')');
                if (ret.status == "False") {
                    spop({
                        template: ret.info,
                        style: 'warning',
                        autoclose: 5000
                    });
                } else {
                    spop({
                        template: ret.info,
                        style: 'success',
                        autoclose: 2000
                    });
                    setTimeout("location.reload()", 2000);
                }
            }
        });
    }
});


///////////////////////机房/////////////////////////////
//添加机房
$("#sub-idc").click(function () {
    var idc_name = $("#idc-name").val();
    var idc_msg = $("#idc-msg").val();
    $.ajax({
        url: "/cmdb/idc/",
        type: 'POST',
        data: {'idc_name': idc_name, 'idc_msg': idc_msg},
        success: function (data) {
            var ret = eval('(' + data + ')');
            if (ret.status == "False") {
                spop({
                    template: ret.info,
                    style: 'warning',
                    autoclose: 5000
                });
            } else {
                $("#idcModal").modal("hide");
                spop({
                    template: ret.info,
                    style: 'success',
                    autoclose: 2000
                });
                setTimeout("location.reload()", 2000);
            }
        }
    })
});

//获取编辑IDC信息
$('td a[name="edit-idc"]').click(function () {
    var idc_id = $(this).attr("idc_id");
    $.ajax({
        url: "/cmdb/idc/",
        type: "PUT",
        data: JSON.stringify({'idc_id': idc_id}),
        success: function (data) {
            var ret = eval('(' + data + ')');
            if (ret.status == "False") {
                spop({
                    template: ret.info,
                    style: 'warning',
                    autoclose: 5000
                });
            } else {
                if (ret.status == "True") {
                    info = ret.info;
                    $("#edit-idc-name").val(info.idc_name);
                    $("#edit-idc-msg").val(info.idc_msg);
                    $("#sub-edit-idc").attr('idc_id', info.idc_id);
                    $("#edit-idcModal").modal('show');
                } else {
                    spop({
                        template: ret.info,
                        autoclose: 2000
                    });
                }

            }

        }
    });
});

//修改idc信息
$("#sub-edit-idc").click(function () {
    var idc_id = $(this).attr("idc_id");
    var idc_name = $("#edit-idc-name").val();
    var idc_msg = $("#edit-idc-msg").val();
    $.ajax({
        url: "/cmdb/idc/",
        type: "PUT",
        data: JSON.stringify({'action': 'edit', 'idc_id': idc_id, 'idc_name': idc_name, 'idc_msg': idc_msg}),
        success: function (data) {
            var ret = eval('(' + data + ')');
            if (ret.status == "False") {
                spop({
                    template: ret.info,
                    style: 'warning',
                    autoclose: 5000
                });
            } else {
                $("#edit-idcModal").modal("hide");
                spop({
                    template: ret.info,
                    style: 'success',
                    autoclose: 2000
                });
                setTimeout("location.reload()", 2000);
            }
        }
    });
});

//删除IDC
$("td a[name='del-idc']").click(function () {
    var idc_id = $(this).attr('idc_id');
    var statu = confirm("是否确认删除！");
    if (statu == true) {
        $.ajax({
            url: "/cmdb/idc/",
            type: "DELETE",
            data: JSON.stringify({'idc_id': idc_id}),
            success: function (data) {
                var ret = eval('(' + data + ')');
                if (ret.status == "False") {
                    spop({
                        template: ret.info,
                        style: 'warning',
                        autoclose: 5000
                    });
                } else {
                    spop({
                        template: ret.info,
                        style: 'success',
                        autoclose: 2000
                    });
                    setTimeout("location.reload()", 2000);
                }
            }
        });
    }
});


///////////////////////资产/////////////////////////////
//添加资产
$("#sub-asset").click(function () {
    var admin_ip = $("#admin-ip").val();
    var login_port = $("#login-port").val();
    var login_user = $("#login-user").val();
    var login_passwd = $("#login-passwd").val();
    var login_id_rsa = $("#login-id-rsa").val();
    var connect_method = $("#connect-method").val();
    var asset_group = $("#asset-group").val();
    var asset_idc = $("#asset-idc").val();
    var asset_msg = $("#asset-msg").val();
    if (admin_ip && login_port && login_user && login_passwd && connect_method) {
        $.ajax({
            url: "/cmdb/asset/",
            type: 'POST',
            data: {
                'admin_ip': admin_ip,
                'login_port': login_port,
                "login_user": login_user,
                "login_passwd": login_passwd,
                "login_id_rsa": login_id_rsa,
                "connect_method": connect_method,
                "asset_group": asset_group,
                "asset_idc": asset_idc,
                "asset_msg": asset_msg,
            },
            success: function (data) {
                var ret = eval('(' + data + ')');
                if (ret.status == "False") {
                    spop({
                        template: ret.info,
                        style: 'warning',
                        autoclose: 5000
                    });
                } else {
                    $("#assetModal").modal("hide");
                    spop({
                        template: ret.info,
                        style: 'success',
                        autoclose: 2000
                    });
                }
            }
        })

    } else {
        spop({
            template: "带*号参数不能为空",
            style: 'warning',
            autoclose: 5000
        });
    }
});

//获取编辑资产信息
$('td a[name="edit-asset"]').click(function () {
    var asset_id = $(this).attr("asset_id");
    $.ajax({
        url: "/cmdb/asset/",
        type: "PUT",
        data: JSON.stringify({'asset_id': asset_id}),
        success: function (data) {
            var ret = eval('(' + data + ')');
            if (ret.status == "False") {
                spop({
                    template: ret.info,
                    style: 'warning',
                    autoclose: 5000
                });
            } else {
                if (ret.status == "True") {
                    info = ret.info;
                    $("#edit-admin-ip").val(info.admin_ip);
                    $("#edit-login-port").val(info.login_port);
                    $("#edit-login-user").val(info.login_user);
                    $("#edit-login-passwd").val(info.login_passwd);
                    $("#edit-login-id-rsa").val(info.login_id_rsa);
                    $("#edit-connect-method").val(info.connect_method);

                    $("#edit-asset-group").val(info.asset_group);
                    $("#edit-asset-idc").val(info.asset_idc);
                    $("#edit-asset-msg").val(info.asset_msg);
                    $("#sub-edit-asset").attr('asset_id', info.asset_id);
                    $("#edit-assetModal").modal('show');
                } else {
                    spop({
                        template: ret.info,
                        autoclose: 2000
                    });
                }
            }
        }
    });
});

//修改资产信息
$("#sub-edit-asset").click(function () {
    var asset_id = $(this).attr("asset_id");
    var admin_ip = $("#edit-admin-ip").val();
    var login_port = $("#edit-login-port").val();
    var login_user = $("#edit-login-user").val();
    var login_passwd = $("#edit-login-passwd").val();
    var login_id_rsa = $("#edit-login-id-rsa").val();
    var connect_method = $("#edit-connect-method").val();

    var asset_group = $("#edit-asset-group").val();
    var asset_idc = $("#edit-asset-idc").val();
    var asset_msg = $("#edit-asset-msg").val();
    $.ajax({
        url: "/cmdb/asset/",
        type: "PUT",
        data: JSON.stringify({
            'action': 'edit',
            'asset_id': asset_id,
            'admin_ip': admin_ip,
            'login_port': login_port,
            "login_user": login_user,
            "login_passwd": login_passwd,
            "login_id_rsa": login_id_rsa,
            "connect_method": connect_method,
            "asset_group": asset_group,
            "asset_idc": asset_idc,
            "asset_msg": asset_msg,
        }),
        success: function (data) {
            var ret = eval('(' + data + ')');
            if (ret.status == "False") {
                spop({
                    template: ret.info,
                    style: 'warning',
                    autoclose: 5000
                });
            } else {
                $("#edit-assetModal").modal("hide");
                spop({
                    template: ret.info,
                    style: 'success',
                    autoclose: 2000
                });
                setTimeout("location.reload()", 2000);
            }
        }
    });
});

//删除资产
$("td a[name='del-asset']").click(function () {
    var asset_id = $(this).attr('asset_id');
    var statu = confirm("是否确认删除！");
    if (statu == true) {
        $.ajax({
            url: "/cmdb/asset/",
            type: "DELETE",
            data: JSON.stringify({'asset_id': asset_id}),
            success: function (data) {
                var ret = eval('(' + data + ')');
                if (ret.status == "False") {
                    spop({
                        template: ret.info,
                        style: 'warning',
                        autoclose: 5000
                    });
                } else {
                    spop({
                        template: ret.info,
                        style: 'success',
                        autoclose: 2000
                    });
                    setTimeout("location.reload()", 2000);
                }
            }
        });
    }
});


//配置连接密钥
$("td a[name='add-connect']").click(function () {
    var asset_id = $(this).attr('asset_id');
    $.ajax({
        url: "/cmdb/setconf/",
        type: "POST",
        data: {'asset_id': asset_id},
        success: function (data) {
            var ret = eval('(' + data + ')');
            if (ret.status == "False") {
                spop({
                    template: ret.info,
                    style: 'warning',
                    autoclose: 5000
                });
            } else {
                spop({
                    template: ret.info,
                    style: 'success',
                    autoclose: 2000
                });
                setTimeout("location.reload()", 2000);
            }
        }
    });
});



//配置连接密钥
$("td a[name='recycling']").click(function () {
    var asset_id = $(this).attr('asset_id');
    $.ajax({
        url: "/cmdb/recycling/",
        type: "POST",
        data: {'asset_id': asset_id},
        success: function (data) {
            var ret = eval('(' + data + ')');
            if (ret.status == "False") {
                spop({
                    template: ret.info,
                    style: 'warning',
                    autoclose: 5000
                });
            } else {
                spop({
                    template: ret.info,
                    style: 'success',
                    autoclose: 2000
                });
                setTimeout("location.reload()", 2000);
            }
        }
    });
});


//关键字查询服务器
$("#sub-search").click(function () {
    var search_key = $("#search-key").val();
    $.post('/cmdb/search/', {'search_key': search_key}, function (data) {
        if (data == "False") {
            spop({
                template: "无权限操作",
                style: 'warning',
                autoclose: 5000
            });
        } else {
            $("#asset-info").empty();
            $("#asset-info").append(data);
        }
    });
});

$("#select-asset-type").click(function () {
    $(this).children().first().attr("disabled", "disabled");
});

//通过服务器类型筛选
$("#select-asset-type").change(function () {
    var type_id = $("#select-asset-type").val();
    $.post('/cmdb/search/', {'type_id': type_id}, function (data) {
        if (data == "False") {
            spop({
                template: "无权限操作",
                style: 'warning',
                autoclose: 5000
            });
        } else {
            $("#asset-info").empty();
            $("#asset-info").append(data);
        }
    });
});


$("#select-hostgroup").click(function () {
    $(this).children().first().attr("disabled", "disabled");
});

//通过分组查询
$("#select-hostgroup").change(function () {
    var group_id = $("#select-hostgroup").val();
    $.post('/cmdb/search/', {'group_id': group_id}, function (data) {
        if (data == "False") {
            spop({
                template: "无权限操作",
                style: 'warning',
                autoclose: 5000
            });
        } else {
            $("#asset-info").empty();
            $("#asset-info").append(data);
        }
    });
});

$("#select-idc").click(function () {
    $(this).children().first().attr("disabled", "disabled");
});

//通过机房查询
$("#select-idc").change(function () {
    var idc_id = $("#select-idc").val();
    $.post('/cmdb/search/', {'idc_id': idc_id}, function (data) {
        if (data == "False") {
            spop({
                template: "无权限操作",
                style: 'warning',
                autoclose: 5000
            });
        } else {
            $("#asset-info").empty();
            $("#asset-info").append(data);
        }
    });
});

//导出服务器信息
$("#export-asset").click(function () {
    $.post('/cmdb/exporthost/', {}, function (data) {
        if (data == "False") {
            spop({
                template: "无权限操作",
                style: 'warning',
                autoclose: 5000
            });
        } else {
            if (data.code == 1) {
                $("#exportModal").modal();
            } else {
                spop({
                    template: data.message,
                    style: 'success',
                    autoclose: 2000
                });
            }
        }
    });
});




