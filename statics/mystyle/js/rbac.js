////////////////////////用户管理////////////////////////////
//添加用户
$("#add-user").click(function(){
    var useranem = $("#username").val();
    var nick_name = $("#nick-name").val();
    var password = $("#password").val();
    var is_superuser = $("#is-superuser").val();
    var role_id = $("#role-id").val();
    $.ajax({
        url:"/rbac/user/",
        type:"POST",
        data:{'username':useranem, 'nick_name':nick_name,'password':password,'role_id':role_id,"is_superuser":is_superuser},
        success:function(data){
            var ret = eval('(' + data + ')');
            if(ret.status =="False"){
                spop({
                    template: ret.info,
                    style: 'warning',
                    autoclose: 5000
                });
            }else {
                $("#userModal").modal("hide");
                spop({
                    template: ret.info,
                    style: 'success',
                    autoclose: 3000
                });
                setTimeout("location.reload()",2000);
            }
        }
    })
});

//获取修改信息
$('td a[name="edit-user"]').click(function(){
    var user_id = $(this).attr("user_id");
        $.ajax({
            url: "/rbac/user/",
            type: "PUT",
            data: JSON.stringify({'user_id':user_id}),
            success: function(data) {
                var ret = eval('(' + data + ')');
                if(ret.status =="False"){
                    spop({
                        template: ret.info,
                        style: 'warning',
                        autoclose: 5000
                    });
                }else {
                    if (ret.status =="True"){
                        info = ret.info;
                        console.log(info);
                        $("#edit-username").val(info.username);
                        $("#edit-nick-name").val(info.nick_name);
                        $("#edit-role-id").val(info.role_id);
                        $("#edit-is-superuser").val(info.is_superuser);
                        $("#sub-edit-user").attr('user_id', info.user_id);
                        $("#edit-userModal").modal('show');
                    }else {
                        spop({
                            template: ret.info,
                            autoclose: 2000
                        });
                    }
                }
        }
    });
});


//修改信息
$("#sub-edit-user").click(function(){
    var user_id = $(this).attr('user_id');
    var username = $("#edit-username").val();
    var nick_name = $("#edit-nick-name").val();
    var is_superuser = $("#edit-is-superuser").val();
    var role_id = $("#edit-role-id").val();
     $.ajax({
        url: "/rbac/user/",
        type: "PUT",
        data: JSON.stringify({'action':'edit','username':username, 'nick_name':nick_name,'role_id':role_id,'user_id':user_id,"is_superuser":is_superuser}),
        success: function(data) {
            var ret = eval('(' + data + ')');
            if(ret.status =="False"){
                spop({
                        template: ret.info,
                        style: 'warning',
                        autoclose: 5000
                    });
            }else {
                $("#edit-userModal").modal("hide");
                spop({
                    template: ret.info,
                    style: 'success',
                    autoclose: 2000
                });
                setTimeout("location.reload()",2000);
            }
        }
    });
});


//修改密码
$("td a[name='ch-passwd']").click(function(){
    var user_id = $(this).attr('user_id');
    $("#new-password").val("");
    $("#passwdModal").modal('show');
    //修改密码
    $("#sub-password").click(function(){
        var new_passwd = $("#new-password").val();
        $.post("/rbac/chpasswd/",{"new_passwd":new_passwd,"user_id":user_id},function(data){
            var ret = eval('(' + data + ')');
            if(ret.status =="Flase"){
                spop({
                    template: ret.info,
                    style: 'warning',
                    autoclose: 5000
                });
            }else {
                $("#passwdModal").modal("hide");
                spop({
                    template: ret.info,
                    style: 'success',
                    autoclose: 2000
                });
                setTimeout("location.reload()",2000);
            }
        });
    });
});



//删除用户
$("td a[name='del-user']").click(function(){
    var user_id = $(this).attr('user_id');
   var statu = confirm("是否确认删除！");
   if (statu==true)
    {
        $.ajax({
            url: "/rbac/user/",
            type: "DELETE",
            data: JSON.stringify({'user_id':user_id}),
            success: function(data) {
                var ret = eval('(' + data + ')');
                if(ret.status =="Flase"){
                    spop({
                        template: ret.info,
                        style: 'warning',
                        autoclose: 2000
                    });
                }else {
                    spop({
                        template: ret.info,
                        style: 'success',
                        autoclose: 2000
                    });
                    setTimeout("location.reload()",2000);
                }
             }
        });
    }
});

////////////////////////角色管理////////////////////////////

//添加角色
$("#sub-role").click(function(){
    var role = $("#role").val();
    $.ajax(
        {
        url:"/rbac/role/",
        type: 'POST',
        data:{'role':role},
        success:function(data) {
            var ret = eval('(' + data + ')');
            if(ret.status =="False"){
                spop({
                    template: ret.info,
                    style: 'warning',
                    autoclose: 5000
                });
            }else {
                $("#roleModal").modal("hide");
                spop({
                    template: ret.info,
                    style: 'success',
                    autoclose: 2000
                });
                setTimeout("location.reload()",2000);
            }
        }
    });
});


//获取修改角色信息
$('td a[name="edit-role"]').click(function(){
    var role_id = $(this).attr("role_id");
    $.ajax({
        url: "/rbac/role/",
        type: "PUT",
        data: JSON.stringify({'role_id':role_id}),
        success: function(data) {
            var ret = eval('(' + data + ')');
            if(ret.status =="False"){
                spop({
                    template: ret.info,
                    style: 'warning',
                    autoclose: 5000
                });
            }else {
                if (ret.status =="True"){
                    info = ret.info;
                    $("#edit-role").val(info.role);
                    $("#sub-edit-role").attr('role_id', info.role_id);
                    $("#edit-roleModal").modal('show');
                }else {
                    spop({
                        template: ret.info,
                        autoclose: 2000
                    });
                }
            }
         }
    });
});

//修改角色信息
$("#sub-edit-role").click(function(){
    var role_id = $(this).attr('role_id');
    var role = $("#edit-role").val();
    $.ajax({
        url: "/rbac/role/",
        type: "PUT",
        data: JSON.stringify({'action':'edit','role':role,'role_id':role_id}),
        success: function(data) {
            var ret = eval('(' + data + ')');
            if(ret.status =="False"){
                spop({
                        template: ret.info,
                        style: 'warning',
                        autoclose: 5000
                    });
            }else {
                $("#edit-roleModal").modal("hide");
                spop({
                    template: ret.info,
                    style: 'success',
                    autoclose: 2000
                });
                setTimeout("location.reload()",2000);
            }
         }
    });
});



//删除角色
$("td a[name='del-role']").click(function(){
   var role_id = $(this).attr('role_id');
   var statu = confirm("是否确认删除！");
   if (statu==true)
    {
        $.ajax({
            url: "/rbac/role/",
            type: "DELETE",
            data: JSON.stringify({'role_id':role_id}),
            success: function(data) {
                var ret = eval('(' + data + ')');
                if(ret.status =="False"){
                    spop({
                        template: ret.info,
                        style: 'warning',
                        autoclose: 5000
                    });
                }else {
                    spop({
                        template: ret.info,
                        style: 'success',
                        autoclose: 2000
                    });
                    setTimeout("location.reload()",2000);
                }
            }
        });
    }
});


////////////////////////权限管理////////////////////////////

//添加权限
$("#sub-perms").click(function(){
    var title = $("#title").val();
    var perms_icon = $("#perms-icon").val();
    var perms_method = $("#perms-method").val();
    var perms_type = $("#perms-type").val();
    var parent_id = $("#parent-id").val();
    var url = $("#url").val();
    var weight = $("#weight").val();
    $.ajax({
            url:"/rbac/perms/",
            type:"POST",
            data:{"title":title,"perms_type":perms_type,"perms_icon":perms_icon, "parent_id":parent_id,'url':url,"perms_method":perms_method,"weight":weight},
            success:function(data){
                var ret = eval('(' + data + ')');
                if(ret.status =="False"){
                    spop({
                        template: ret.info,
                        style: 'warning',
                        autoclose: 5000
                    });
                }else {
                    $("#permsModal").modal("hide");
                    spop({
                        template: ret.info,
                        style: 'success',
                        autoclose: 2000
                    });
                    setTimeout("location.reload()",2000);
                }
            }
        });
});


//获取编辑权限信息
$('td a[name="edit-perms"]').click(function() {
    var perms_id = $(this).attr("perms_id");
    $.ajax({
        url: "/rbac/perms/",
        type: "PUT",
        data: JSON.stringify({'perms_id': perms_id}),
        success: function (data) {
            var ret = eval('(' + data + ')');
                if(ret.status =="Flase"){
                    spop({
                        template: ret.info,
                        style: 'warning',
                        autoclose: 5000
                    });
                }else {
                    if (ret.status == "True") {
                        info = ret.info;
                        console.log(info);
                         $("#edit-title").val(info.title);
                         $("#edit-perms-icon").val(info.perms_icon);
                         $("#edit-perms-method").val(info.perms_method);
                         $("#edit-perms-type").val(info.perms_type);
                         $("#edit-parent-id").val(info.parent_id);
                         $("#edit-url").val(info.url );
                         $("#edit-weight").val(info.weight);
                        $("#edit-sub-perms").attr('perms_id', info.perms_id);
                        $("#edit-permsModal").modal('show');
                    } else {
                        spop({
                            template: ret.info,
                            autoclose: 2000
                        });
                    }
                }
        }
    });
});

//修改权限信息
$("#edit-sub-perms").click(function() {
    var perms_id = $(this).attr("perms_id");
    var title = $("#edit-title").val();
    var perms_icon = $("#edit-perms-icon").val();
    var perms_method = $("#edit-perms-method").val();
    var perms_type = $("#edit-perms-type").val();
    var parent_id = $("#edit-parent-id").val();
    var url = $("#edit-url").val();
    var weight = $("#edit-weight").val();
    $.ajax({
        url: "/rbac/perms/",
        type: "PUT",
        data: JSON.stringify({"action":"edit","perms_id":perms_id,"title":title,"perms_type":perms_type,"perms_icon":perms_icon, "parent_id":parent_id,'url':url,"perms_method":perms_method,"weight":weight}),
        success: function (data) {
            var ret = eval('(' + data + ')');
            if(ret.status =="Flase"){
                spop({
                        template: ret.info,
                        style: 'warning',
                        autoclose: 5000
                    });
            }else {
                $("#edit-permsModal").modal("hide");
                spop({
                    template: ret.info,
                    style: 'success',
                    autoclose: 2000
                });
                setTimeout("location.reload()",2000);
            }
        }
    });
});


//删除权限
$("td a[name='del-perms']").click(function(){
    var perms_id = $(this).attr('perms_id');
    var statu = confirm("是否确认删除！");
    if (statu==true)
    {
        $.ajax({
            url: "/rbac/perms/",
            type: "DELETE",
            data: JSON.stringify({'perms_id':perms_id}),
            success: function(data) {
                var ret = eval('(' + data + ')');
                if(ret.status =="Flase"){
                    spop({
                        template: ret.info,
                        style: 'warning',
                        autoclose: 5000
                    });
                }else {
                    spop({
                        template: ret.info,
                        style: 'success',
                        autoclose: 2000
                    });
                    setTimeout("location.reload()",2000);
                }
             }
        });
    }
});


////////////////////////配置字典////////////////////////////

//添加配置
$("#sub-dict").click(function(){
    var dict_key = $("#dict-key").val();
    var dict_val = $("#dict-val").val();
    var dict_msg = $("#dict-msg").val();
    $.ajax(
        {
        url:"/rbac/dict/",
        type: 'POST',
        data:{'dict_key':dict_key,"dict_val":dict_val,"dict_msg":dict_msg},
        success:function(data) {
            var ret = eval('(' + data + ')');
            if(ret.status =="False"){
                spop({
                    template: ret.info,
                    style: 'warning',
                    autoclose: 5000
                });
            }else {
                $("#dictModal").modal("hide");
                spop({
                    template: ret.info,
                    style: 'success',
                    autoclose: 2000
                });
                setTimeout("location.reload()",2000);
            }
        }
    });
});


//获取修改
$('td a[name="edit-dict"]').click(function(){
    var dict_id = $(this).attr("dict_id");
    $.ajax({
        url: "/rbac/dict/",
        type: "PUT",
        data: JSON.stringify({'dict_id':dict_id}),
        success: function(data) {
            var ret = eval('(' + data + ')');
            if(ret.status =="False"){
                spop({
                    template: ret.info,
                    style: 'warning',
                    autoclose: 5000
                });
            }else {
                if (ret.status =="True"){
                    info = ret.info;
                    $("#edit-dict-key").val(info.dict_key);
                    $("#edit-dict-val").val(info.dict_val);
                    $("#edit-dict-msg").val(info.dict_msg);
                    $("#sub-edit-dict").attr('dict_id', info.dict_id);
                    $("#edit-dictModal").modal('show');
                }else {
                    spop({
                        template: ret.info,
                        autoclose: 2000
                    });
                }
            }
         }
    });
});

//修改
$("#sub-edit-dict").click(function(){
    var dict_id = $(this).attr('dict_id');
    var dict_key = $("#edit-dict-key").val();
    var dict_val = $("#edit-dict-val").val();
    var dict_msg = $("#edit-dict-msg").val();
    $.ajax({
        url: "/rbac/dict/",
        type: "PUT",
        data: JSON.stringify({'action':'edit','dict_key':dict_key,"dict_val":dict_val,"dict_msg":dict_msg,'dict_id':dict_id}),
        success: function(data) {
            var ret = eval('(' + data + ')');
            if(ret.status =="False"){
                spop({
                        template: ret.info,
                        style: 'warning',
                        autoclose: 5000
                    });
            }else {
                $("#edit-dictModal").modal("hide");
                spop({
                    template: ret.info,
                    style: 'success',
                    autoclose: 2000
                });
                setTimeout("location.reload()",2000);
            }
         }
    });
});



//删除
$("td a[name='del-dict']").click(function(){
   var dict_id = $(this).attr('dict_id');
   var statu = confirm("是否确认删除！");
   if (statu==true)
    {
        $.ajax({
            url: "/rbac/dict/",
            type: "DELETE",
            data: JSON.stringify({'dict_id':dict_id}),
            success: function(data) {
                var ret = eval('(' + data + ')');
                if(ret.status =="False"){
                    spop({
                        template: ret.info,
                        style: 'warning',
                        autoclose: 5000
                    });
                }else {
                    spop({
                        template: ret.info,
                        style: 'success',
                        autoclose: 2000
                    });
                    setTimeout("location.reload()",2000);
                }
            }
        });
    }
});





