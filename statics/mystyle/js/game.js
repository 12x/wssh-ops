/////////////////////////web///////////////////////////////
//添加web
$("#sub-web").click(function(){
    var platform_id = $("#web-platform").val();
    var asset_id = $("#asset-id").val();
    $.ajax({
        url: "/game/wlist/",
        type: 'POST',
        data: {"platform_id": platform_id,"asset_id":asset_id},
        success: function (data) {
            var ret = eval('(' + data + ')');
            if(ret.status =="False"){
                spop({
                    template:ret.info,
                    style: 'warning',
                    autoclose: 2000
                });
            }else {
                $("#webModal").modal("hide");
                spop({
                    template: ret.info,
                    style: 'success',
                    autoclose: 2000
                });
                setTimeout("location.reload()",2000);
            }
        }
    })
});

//删除
$("td a[name='del-web']").click(function(){
    var web_id = $(this).attr('web_id');
    var statu = confirm("是否确认删除！");
    if (statu==true)
    {
        $.ajax({
            url: "/game/wlist/",
            type: "DELETE",
            data: JSON.stringify({'web_id':web_id}),
            success: function(data) {
                var ret = eval('(' + data + ')');
                if(ret.status =="False"){
                    spop({
                        template: ret.info,
                        style: 'warning',
                        autoclose: 2000
                    });
                }else {
                    spop({
                        template: ret.info,
                        style: 'success',
                        autoclose: 2000
                    });
                    setTimeout("location.reload()",2000);
                }
             }
        });
    }
});

//运行web
$("td a[name='run-web']").click(function(){
    var web_id = $(this).attr('web_id');
    $.ajax({
        url: "/game/wrun/",
        type: "POST",
        data: {'web_id':web_id},
        success: function(data) {
            var ret = eval('(' + data + ')');
            if(ret.status =="False"){
                spop({
                    template: ret.info,
                    style: 'warning',
                    autoclose: 2000
                });
            }else {
                spop({
                    template: ret.info,
                    style: 'success',
                    autoclose: 2000
                });
                setTimeout("location.reload()",2000);
            }
         }
    });

});

//停止web
$("td a[name='stop-web']").click(function(){
    var web_id = $(this).attr('web_id');
    $.ajax({
        url: "/game/wstop/",
        type: "POST",
        data: {'web_id':web_id},
        success: function(data) {
            var ret = eval('(' + data + ')');
            if(ret.status =="False"){
                spop({
                    template: ret.info,
                    style: 'warning',
                    autoclose: 2000
                });
            }else {
                spop({
                    template: ret.info,
                    style: 'success',
                    autoclose: 2000
                });
                setTimeout("location.reload()",2000);
            }
         }
    });
});


//更新web
$("td a[name='up-web']").click(function(){
    var web_id = $(this).attr('web_id');
    $.ajax({
        url: "/game/wupdate/",
        type: "POST",
        data: {'web_id':web_id},
        success: function(data) {
            var ret = eval('(' + data + ')');
            if(ret.status =="False"){
                spop({
                    template: ret.info,
                    style: 'warning',
                    autoclose: 2000
                });
            }else {
                spop({
                    template: ret.info,
                    style: 'success',
                    autoclose: 2000
                });
                setTimeout("location.reload()",2000);
            }
         }
    });
});


//批量运行web
$("#sub-run-web").click(function() {
    var platform_id = $("#run-web-platform").val();
    var exp_host = $("#run-exp-host").val();
    $.ajax({
        url: "/game/mgweb/",
        type: "POST",
        data: {'action':'run','platform_id':platform_id,"exp_host":exp_host},
        success: function (data) {
            var ret = eval('(' + data + ')');
            if(ret.status =="False"){
                spop({
                        template: ret.info,
                        style: 'warning',
                        autoclose: 2000
                    });
            }else {
                $("#runwebModal").modal("hide");
                spop({
                    template: ret.info,
                    style: 'success',
                    autoclose: 2000
                });
                setTimeout("location.reload()",2000);
            }
        }
    });
});


//批量 web stop
$("#sub-stop-web").click(function() {
    var platform_id = $("#stop-web-platform").val();
    var exp_host = $("#stop-exp-host").val();
    $.ajax({
        url: "/game/mgweb/",
        type: "POST",
        data: {'action':'stop','platform_id':platform_id,"exp_host":exp_host},
        success: function (data) {
            var ret = eval('(' + data + ')');
            if(ret.status =="False"){
                spop({
                        template: ret.info,
                        style: 'warning',
                        autoclose: 2000
                    });
            }else {
                $("#stopwebModal").modal("hide");
                spop({
                    template: ret.info,
                    style: 'success',
                    autoclose: 2000
                });
                setTimeout("location.reload()",2000);
            }
        }
    });
});


//web up
$("#sub-up-web").click(function() {
    var platform_id = $("#up-web-platform").val();
    var exp_host = $("#up-exp-host").val();
    $.ajax({
        url: "/game/mgweb/",
        type: "POST",
        data: {'action':'up','platform_id':platform_id,"exp_host":exp_host},
        success: function (data) {
            var ret = eval('(' + data + ')');
            if(ret.status =="False"){
                spop({
                        template: ret.info,
                        style: 'warning',
                        autoclose: 2000
                    });
            }else {
                $("#upwebModal").modal("hide");
                spop({
                    template: ret.info,
                    style: 'success',
                    autoclose: 2000
                });
                setTimeout("location.reload()",2000);
            }
        }
    });
});


$("#search-web-platform").click(function () {
    $(this).children().first().attr("disabled","disabled");
});

//通过分组查询web
$("#search-web-platform").change(function(){
    var platform_id = $(this).val();
    $.post('/game/wsearch/',{'platform_id':platform_id},function(data){
        if(data=="False"){
            spop({
                template: "无权限操作",
                style: 'warning',
                autoclose: 2000
                });
        }else {
            $("#web-info").empty();
            $("#web-info").append(data);
        }
    });
});

$("#web-host").click(function () {
    $(this).children().first().attr("disabled","disabled");
});

//通过服务器查询
$("#web-host").change(function(){
    var host_id = $(this).val();
    $.post('/game/wsearch/',{'host_id':host_id},function(data){
        if(data=="False"){
            spop({
                template: "无权限操作",
                style: 'warning',
                autoclose: 2000
                });
        }else {
            $("#web-info").empty();
            $("#web-info").append(data);
        }
    });
});



/////////////////////////cross/////////////////////////////////
//添加
$("#sub-cross").click(function(){
    var platform_id = $("#cross-platform").val();
    var asset_id = $("#asset-id").val();
    var cid = $("#cross-id").val();
    var cross_node = $("#cross-node").val();
    $.ajax({
        url: "/game/clist/",
        type: 'POST',
        data: {"platform_id": platform_id,"asset_id":asset_id,'cid':cid,'cross_node':cross_node},
        success: function (data) {
            var ret = eval('(' + data + ')');
            if(ret.status =="False"){
                spop({
                    template:ret.info,
                    style: 'warning',
                    autoclose: 5000
                });
            }else {
                $("#crossModal").modal("hide");
                spop({
                    template: ret.info,
                    style: 'success',
                    autoclose: 2000
                });
                setTimeout("location.reload()",1000);
            }
        }
    })
});


//获取编辑信息
$('td a[name="edit-cross"]').click(function() {
    var cross_id = $(this).attr("cross_id");
    $.ajax({
        url: "/game/clist/",
        type: "PUT",
        data: JSON.stringify({'cross_id': cross_id}),
        success: function (data) {
            var ret = eval('(' + data + ')');
            if(ret.status =="False"){
                spop({
                    template: ret.info,
                    style: 'warning',
                    autoclose: 2000
                });
            }else {
                if (ret.status =="True"){
                    info = ret.info;
                    $("#edit-cross-group").val(info.cross_group);
                    $("#edit-cross-dist").val(info.cross_dist);
                    $("#sub-edit-cross").attr('cross_id',info.cross_id);
                    $("#edit-crossModal").modal('show');
                }else {
                    spop({
                        template: ret.info,
                        autoclose: 2000
                    });
                }

            }

        }
    });
});

//修改信息
$("#sub-edit-cross").click(function() {
    var cross_id = $("#sub-edit-cross").attr('cross_id');
    var cross_group = $("#edit-cross-group").val();
    var cross_dist = $("#edit-cross-dist").val();
    $.ajax({
        url: "/game/clist/",
        type: "PUT",
        data: JSON.stringify({'action':'edit','cross_id':cross_id,'cross_group':cross_group,'cross_dist':cross_dist}),
        success: function (data) {
            var ret = eval('(' + data + ')');
            if(ret.status =="False"){
                spop({
                        template: ret.info,
                        style: 'warning',
                        autoclose: 2000
                    });
            }else {
                $("#edit-crossModal").modal("hide");
                spop({
                    template: ret.info,
                    style: 'success',
                    autoclose: 2000
                });
                setTimeout("location.reload()",2000);
            }
        }
    });
});


//删除
$("td a[name='del-cross']").click(function(){
    var cross_id = $(this).attr('cross_id');
    var statu = confirm("是否确认删除！");
    if (statu==true)
    {
        $.ajax({
            url: "/game/clist/",
            type: "DELETE",
            data: JSON.stringify({'cross_id':cross_id}),
            success: function(data) {
                var ret = eval('(' + data + ')');
                if(ret.status =="False"){
                    spop({
                        template: ret.info,
                        style: 'warning',
                        autoclose: 2000
                    });
                }else {
                    spop({
                        template: ret.info,
                        style: 'success',
                        autoclose: 2000
                    });
                    setTimeout("location.reload()",2000);
                }
             }
        });
    }
});


//运行cross
$("td a[name='run-cross']").click(function(){
    var cross_id = $(this).attr('cross_id');
    $.ajax({
        url: "/game/crun/",
        type: "POST",
        data: {'cross_id':cross_id},
        success: function(data) {
            var ret = eval('(' + data + ')');
            if(ret.status =="False"){
                spop({
                    template: ret.info,
                    style: 'warning',
                    autoclose: 2000
                });
            }else {
                spop({
                    template: ret.info,
                    style: 'success',
                    autoclose: 2000
                });
                setTimeout("location.reload()",2000);
            }
         }
    });

});

//停止cross
$("td a[name='stop-cross']").click(function(){
    var cross_id = $(this).attr('cross_id');
    $.ajax({
        url: "/game/cstop/",
        type: "POST",
        data: {'cross_id':cross_id},
        success: function(data) {
            var ret = eval('(' + data + ')');
            if(ret.status =="False"){
                spop({
                    template: ret.info,
                    style: 'warning',
                    autoclose: 2000
                });
            }else {
                spop({
                    template: ret.info,
                    style: 'success',
                    autoclose: 2000
                });
                setTimeout("location.reload()",2000);
            }
         }
    });
});


//更新cross
$("td a[name='up-cross']").click(function(){
    var cross_id = $(this).attr('cross_id');
    $.ajax({
        url: "/game/cupdate/",
        type: "POST",
        data: {'cross_id':cross_id},
        success: function(data) {
            var ret = eval('(' + data + ')');
            if(ret.status =="False"){
                spop({
                    template: ret.info,
                    style: 'warning',
                    autoclose: 2000
                });
            }else {
                spop({
                    template: ret.info,
                    style: 'success',
                    autoclose: 2000
                });
                setTimeout("location.reload()",2000);
            }
         }
    });
});


//批量运行cross
$("#sub-run-cross").click(function() {
    var platform_id = $("#run-cross-platform").val();
    var start_id = $("#cross-run-start").val();
    var end_id = $("#cross-run-end").val();
    $.ajax({
        url: "/game/mgcross/",
        type: "POST",
        data: {'action':'run','platform_id':platform_id,"start_id":start_id,"end_id":end_id},
        success: function (data) {
            var ret = eval('(' + data + ')');
            if(ret.status =="False"){
                spop({
                        template: ret.info,
                        style: 'warning',
                        autoclose: 2000
                    });
            }else {
                $("#runcrossModal").modal("hide");
                spop({
                    template: ret.info,
                    style: 'success',
                    autoclose: 2000
                });
                setTimeout("location.reload()",2000);
            }
        }
    });
});


//批量停止cross
$("#sub-stop-cross").click(function() {
    var platform_id = $("#stop-cross-platform").val();
    var start_id = $("#cross-stop-start").val();
    var end_id = $("#cross-stop-end").val();
    $.ajax({
        url: "/game/mgcross/",
        type: "POST",
        data: {'action':'stop','platform_id':platform_id,"start_id":start_id,"end_id":end_id},
        success: function (data) {
            var ret = eval('(' + data + ')');
            if(ret.status =="False"){
                spop({
                        template: ret.info,
                        style: 'warning',
                        autoclose: 2000
                    });
            }else {
                $("#stopcrossModal").modal("hide");
                spop({
                    template: ret.info,
                    style: 'success',
                    autoclose: 2000
                });
                setTimeout("location.reload()",2000);
            }
        }
    });
});


//批量更新cross
$("#sub-up-cross").click(function() {
    var platform_id = $("#up-cross-platform").val();
    var start_id = $("#cross-up-start").val();
    var end_id = $("#cross-up-end").val();
    $.ajax({
        url: "/game/mgcross/",
        type: "POST",
        data: {'action':'up','platform_id':platform_id,"start_id":start_id,"end_id":end_id},
        success: function (data) {
            var ret = eval('(' + data + ')');
            if(ret.status =="False"){
                spop({
                        template: ret.info,
                        style: 'warning',
                        autoclose: 2000
                    });
            }else {
                $("#upcrossModal").modal("hide");
                spop({
                    template: ret.info,
                    style: 'success',
                    autoclose: 2000
                });
                setTimeout("location.reload()",2000);
            }
        }
    });
});


$("#search-cross-platform").click(function () {
    $(this).children().first().attr("disabled","disabled");
});

//通过分组查询
$("#search-cross-platform").change(function(){
    var platform_id = $(this).val();
    $.post('/game/csearch/',{'platform_id':platform_id},function(data){
        if(data=="False"){
            spop({
                template: "无权限操作",
                style: 'warning',
                autoclose: 2000
                });
        }else {
            $("#cross-info").empty();
            $("#cross-info").append(data);
        }
    });
});

$("#cross-host").click(function () {
    $(this).children().first().attr("disabled","disabled");
});

//通过服务器查询
$("#cross-host").change(function(){
    var host_id = $("#cross-host").val();
    $.post('/game/csearch/',{'host_id':host_id},function(data){
        if(data=="False"){
            spop({
                template: "无权限操作",
                style: 'warning',
                autoclose: 2000
                });
        }else {
            $("#cross-info").empty();
            $("#cross-info").append(data);
        }
    });
});


$("#cross-cid").click(function () {
    $(this).children().first().attr("disabled","disabled");
});

//通过
$("#cross-cid").change(function(){
    var cross_cid= $("#cross-cid").val();
    $.post('/game/csearch/',{'cross_cid':cross_cid},function(data){
        if(data=="False"){
            spop({
                template: "无权限操作",
                style: 'warning',
                autoclose: 2000
                });
        }else {
            $("#cross-info").empty();
            $("#cross-info").append(data);
        }
    });
});




/////////////////////////添加游戏服/////////////////////////////////
//添加
$("#sub-server").click(function(){
    var platform_id = $("#game-platform").val();
    var asset_id = $("#asset-id").val();
    var sid = $("#game-server-id").val();
    var cross_str = $("#cross-id-str").val();
    var open_time = $("#open-time").val();
    var pf_id = $("#game-pf-id").val();
    var client_version = $("#client-version").val();
    var open_step = $("#open-step").val();
    $.ajax({
        url: "/game/glist/",
        type: 'POST',
        data: {"platform_id": platform_id,"sid":sid,"cross_str":cross_str,"asset_id":asset_id,
            "open_time":open_time,"pf_id":pf_id,"client_version":client_version,"open_step":open_step},
        success: function (data) {
            var ret = eval('(' + data + ')');
            if(ret.status =="False"){
                spop({
                    template:ret.info,
                    style: 'warning',
                    autoclose: 5000
                });
            }else {
                $("#serverModal").modal("hide");
                spop({
                    template: ret.info,
                    style: 'success',
                    autoclose: 2000
                });
                setTimeout("location.reload()",2000);
            }
        }
    })
});

//获取编辑信息
$('td a[name="edit-server"]').click(function() {
    var server_id = $(this).attr("server_id");
    $.ajax({
        url: "/game/glist/",
        type: "PUT",
        data: JSON.stringify({'server_id': server_id}),
        success: function (data) {
            var ret = eval('(' + data + ')');
            if(ret.status =="False"){
                spop({
                    template: ret.info,
                    style: 'warning',
                    autoclose: 2000
                });
            }else {
                if (ret.status =="True"){
                    info = ret.info;
                    $("#edit-game-platform").val(info.game_platform);
                    $("#edit-asset-id").val(info.asset_id);
                    $("#edit-game-server-id").val(info.sid);
                    $("#edit-cross-group").empty();
                    $("#edit-cross-group").append(info.cross_group_info);
                    $("#edit-cross-dist").empty();
                    $("#edit-cross-dist").append(info.cross_dist_info);
                    $("#edit-merge-id").val(info.merge_id);
                    $("#edit-merge-times").val(info.merge_times);
                    $("#edit-open-time").val(info.open_time);
                    $("#sub-edit-server").attr('server_id',info.server_id);
                    $("#edit-serverModal").modal('show');
                }else {
                    spop({
                        template: ret.info,
                        autoclose: 2000
                    });
                }

            }

        }
    });
});


//修改信息
$("#sub-edit-server").click(function() {
    var server_id = $("#sub-edit-server").attr('server_id');
    var platform_id = $("#edit-game-platform").val();
    var host_id = $("#edit-asset-id").val();
    var sid = $("#edit-game-server-id").val();
    var cross_group = $("#edit-cross-group").val();
    var cross_dist = $("#edit-cross-dist").val();
    var merge_id = $("#edit-merge-id").val();
    var merge_times = $("#edit-merge-times").val();
    var open_time = $("#edit-open-time").val();
    if(cross_group == null || cross_group == undefined){
        cross_group = 0
    };
    if(cross_dist == null || cross_dist == undefined){
        cross_dist = 0
    };
    $.ajax({
        url: "/game/glist/",
        type: "PUT",
        data: JSON.stringify({'action':'edit','server_id':server_id,'platform_id':platform_id,'host_id':host_id,
            'sid':sid,'cross_group':cross_group,'cross_dist':cross_dist,'open_time':open_time,"merge_id":merge_id,"merge_times":merge_times}),
        success: function (data) {
            var ret = eval('(' + data + ')');
            if(ret.status == "False"){
                spop({
                        template: ret.info,
                        style: 'warning',
                        autoclose: 2000
                    });
            }else {
                $("#edit-serverModal").modal("hide");
                spop({
                    template: ret.info,
                    style: 'success',
                    autoclose: 2000
                });
                setTimeout("location.reload()",2000);
            }
        }
    });
});

//删除
$("td a[name='del-server']").click(function(){
    var server_id = $(this).attr('server_id');
    var statu = confirm("是否确认删除！");
    if (statu==true)
    {
        $.ajax({
            url: "/game/glist/",
            type: "DELETE",
            data: JSON.stringify({'server_id':server_id}),
            success: function(data) {
                var ret = eval('(' + data + ')');
                if(ret.status =="False"){
                    spop({
                        template: ret.info,
                        style: 'warning',
                        autoclose: 2000
                    });
                }else {
                    spop({
                        template: ret.info,
                        style: 'success',
                        autoclose: 2000
                    });
                    setTimeout("location.reload()",2000);
                }
             }
        });
    }
});


//运行服务
$('td a[name="run-server"]').click(function() {
    var game_id = $(this).attr("server_id");
    $.ajax({
        url: "/game/grun/",
        type: "POST",
        data: {'game_id': game_id},
        success: function (data) {
            var ret = eval('(' + data + ')');
            if(ret.status =="False"){
                spop({
                    template: ret.info,
                    style: 'warning',
                    autoclose: 2000
                });
            }else {
                    spop({
                        template: ret.info,
                        autoclose: 2000
                    });
                    setTimeout("location.reload()",2000);
                }
        }
    });
});


//停止服务
$("td a[name='stop-server']").click(function(){
    var game_id = $(this).attr('server_id');
    $.ajax({
        url: "/game/gstop/",
        type: "POST",
        data: {'game_id':game_id},
        success: function(data) {
            var ret = eval('(' + data + ')');
            if(ret.status =="False"){
                spop({
                    template: ret.info,
                    style: 'warning',
                    autoclose: 2000
                });
            }else {
                spop({
                    template: ret.info,
                    style: 'success',
                    autoclose: 2000
                });
                setTimeout("location.reload()",2000);
            }
         }
    });
});


//开服
$("td a[name='open-server']").click(function(){
    var game_id = $(this).attr('server_id');
    $.ajax({
        url: "/game/gopen/",
        type: "POST",
        data: {'game_id':game_id},
        success: function(data) {
            var ret = eval('(' + data + ')');
            if(ret.status =="False"){
                spop({
                    template: ret.info,
                    style: 'warning',
                    autoclose: 2000
                });
            }else {
                spop({
                    template: ret.info,
                    style: 'success',
                    autoclose: 2000
                });
                setTimeout("location.reload()",2000);
            }
         }
    });
});



//关闭入口
$("td a[name='close-server']").click(function(){
    var game_id = $(this).attr('server_id');
    $.ajax({
        url: "/game/gclose/",
        type: "POST",
        data: {'game_id':game_id},
        success: function(data) {
            var ret = eval('(' + data + ')');
            if(ret.status =="False"){
                spop({
                    template: ret.info,
                    style: 'warning',
                    autoclose: 2000
                });
            }else {
                spop({
                    template: ret.info,
                    style: 'success',
                    autoclose: 2000
                });
                setTimeout("location.reload()",2000);
            }
         }
    });
});


//合服
$("#sub-merge-server").click(function() {
    var platform_id = $("#merge-game-platform").val();
    var merge_info = $("#merge-info").val();
    $.ajax({
        url: "/game/gmerge/",
        type: "POST",
        data: {'platform_id':platform_id,"merge_info":merge_info},
        success: function (data) {
            var ret = eval('(' + data + ')');
            if(ret.status =="False"){
                spop({
                        template: ret.info,
                        style: 'warning',
                        autoclose: 5000
                    });
            }else {
                $("#merge-serverModal").modal("hide");
                spop({
                    template: ret.info,
                    style: 'success',
                    autoclose: 2000
                });
                setTimeout("location.reload()",2000);
            }
        }
    });
});


//更新游戏
$('td a[name="up-server"]').click(function() {
    var game_id = $(this).attr("server_id");
    $.ajax({
        url: "/game/gupdate/",
        type: "POST",
        data: {'game_id': game_id},
        success: function (data) {
            var ret = eval('(' + data + ')');
            if(ret.status =="False"){
                spop({
                    template: ret.info,
                    style: 'warning',
                    autoclose: 2000
                });
            }else {
                    spop({
                        template: ret.info,
                        autoclose: 2000
                    });
                    setTimeout("location.reload()",2000);
                }
        }
    });
});


//批量运行
$("#sub-run-game").click(function() {
    var platform_id = $("#run-game-platform").val();
    var start_id = $("#run-start").val();
    var end_id = $("#run-end").val();
    $.ajax({
        url: "/game/mggame/",
        type: "POST",
        data: {'action':'run','platform_id':platform_id,'start_id':start_id,'end_id':end_id},
        success: function (data) {
            var ret = eval('(' + data + ')');
            if(ret.status =="False"){
                spop({
                        template: ret.info,
                        style: 'warning',
                        autoclose: 2000
                    });
            }else {
                $("#rungameModal").modal("hide");
                spop({
                    template: ret.info,
                    style: 'success',
                    autoclose: 2000
                });
                setTimeout("location.reload()",2000);
            }
        }
    });
});


//批量停止game
$("#sub-stop-game").click(function() {
    var platform_id = $("#stop-game-platform").val();
    var start_id = $("#stop-start").val();
    var end_id = $("#stop-end").val();
    $.ajax({
        url: "/game/mggame/",
        type: "POST",
        data: {'action':'stop','platform_id':platform_id,'start_id':start_id,'end_id':end_id},
        success: function (data) {
            var ret = eval('(' + data + ')');
            if(ret.status =="False"){
                spop({
                        template: ret.info,
                        style: 'warning',
                        autoclose: 2000
                    });
            }else {
                $("#stopgameModal").modal("hide");
                spop({
                    template: ret.info,
                    style: 'success',
                    autoclose: 2000
                });
                setTimeout("location.reload()",2000);
            }
        }
    });
});


//批量更新game
$("#sub-up-game").click(function() {
    var platform_id = $("#up-game-platform").val();
    var start_id = $("#up-start").val();
    var end_id = $("#up-end").val();
    $.ajax({
        url: "/game/mggame/",
        type: "POST",
        data: {'action':'up','platform_id':platform_id,'start_id':start_id,'end_id':end_id},
        success: function (data) {
            var ret = eval('(' + data + ')');
            if(ret.status =="False"){
                spop({
                        template: ret.info,
                        style: 'warning',
                        autoclose: 2000
                    });
            }else {
                $("#upgameModal").modal("hide");
                spop({
                    template: ret.info,
                    style: 'success',
                    autoclose: 2000
                });
                setTimeout("location.reload()",2000);
            }
        }
    });
});

//批量开服
$("#sub-open-game").click(function() {
    var platform_id = $("#open-game-platform").val();
    var start_id = $("#open-start").val();
    var end_id = $("#open-end").val();
    $.ajax({
        url: "/game/mggame/",
        type: "POST",
        data: {'action':'open','platform_id':platform_id,'start_id':start_id,'end_id':end_id},
        success: function (data) {
            var ret = eval('(' + data + ')');
            if(ret.status =="False"){
                spop({
                        template: ret.info,
                        style: 'warning',
                        autoclose: 2000
                    });
            }else {
                $("#opengameModal").modal("hide");
                spop({
                    template: ret.info,
                    style: 'success',
                    autoclose: 2000
                });
                setTimeout("location.reload()",2000);
            }
        }
    });
});


//批量关闭入口
$("#sub-close-game").click(function() {
    var platform_id = $("#close-game-platform").val();
    var start_id = $("#close-start").val();
    var end_id = $("#close-end").val();
    $.ajax({
        url: "/game/mggame/",
        type: "POST",
        data: {'action':'close','platform_id':platform_id,'start_id':start_id,'end_id':end_id},
        success: function (data) {
            var ret = eval('(' + data + ')');
            if(ret.status =="False"){
                spop({
                        template: ret.info,
                        style: 'warning',
                        autoclose: 2000
                    });
            }else {
                $("#closegameModal").modal("hide");
                spop({
                    template: ret.info,
                    style: 'success',
                    autoclose: 2000
                });
                setTimeout("location.reload()",2000);
            }
        }
    });
});


//热更更新
$('td a[name="hotup-server"]').click(function() {
    var sid = $(this).attr("sid");
    var platform_id = $(this).attr("platform_id");
    $("#hotup-game-platform").val(platform_id);
    $("#hotup-start").val(sid);
    $("#hotup-end").val(sid);
    $("#hotup-game-platform").attr("disabled","disabled");
    $("#hotup-start").attr("disabled","disabled");
    $("#hotup-end").attr("disabled","disabled");
    $("#hotupgameModal").modal("show");
});


//热更更新
$('#game-hotupdate').click(function() {
    $("#hotup-start").val(null);
    $("#hotup-end").val(null);
    $("#hotup-game-platform").removeAttr("disabled");
    $("#hotup-start").removeAttr("disabled");
    $("#hotup-end").removeAttr("disabled");
    $("#hotupgameModal").modal("show");
});


//关键字查询服务器
$("#sub-search").click(function(){
    var search_key= $("#search-key").val();
    $.post('/game/gamesearch/',{'search_key':search_key},function(data){
        if(data=="False"){
            spop({
                template: "无权限操作",
                style: 'warning',
                autoclose: 2000
                });
        }else {
            $("#game-info").empty();
            $("#game-info").append(data);
        }
    });
});



$("#search-game-platform").click(function () {
    $(this).children().first().attr("disabled","disabled");
});

//通过平台查询
$("#search-game-platform").change(function(){
    var platform_id = $(this).val();
    $.post('/game/gamesearch/',{'platform_id':platform_id},function(data){
        if(data=="False"){
            spop({
                template: "无权限操作",
                style: 'warning',
                autoclose: 2000
                });
        }else {
            $("#game-info").empty();
            $("#game-info").append(data);
        }
    });
});

$("#select-host").click(function () {
    $(this).children().first().attr("disabled","disabled");
});

//通过机房查询
$("#select-host").change(function(){
    var host_id = $("#select-host").val();
    $.post('/game/gamesearch/',{'host_id':host_id},function(data){
        if(data=="False"){
            spop({
                template: "无权限操作",
                style: 'warning',
                autoclose: 2000
                });
        }else {
            $("#game-info").empty();
            $("#game-info").append(data);
        }
    });
});


$("#select-cross-group").click(function () {
    $(this).children().first().attr("disabled","disabled");
});

//通过
$("#select-cross-group").change(function(){
    var cross_group= $("#select-cross-group").val();
    $.post('/game/gamesearch/',{'cross_group':cross_group},function(data){
        if(data=="False"){
            spop({
                template: "无权限操作",
                style: 'warning',
                autoclose: 2000
                });
        }else {
            $("#game-info").empty();
            $("#game-info").append(data);
        }
    });
});


