/*
 Navicat Premium Data Transfer

 Source Server         : 192.168.1.134
 Source Server Type    : MySQL
 Source Server Version : 50731
 Source Host           : 192.168.1.134:3306
 Source Schema         : wsshOps

 Target Server Type    : MySQL
 Target Server Version : 50731
 File Encoding         : 65001

 Date: 12/10/2020 16:01:45
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for auth_group
-- ----------------------------
DROP TABLE IF EXISTS `auth_group`;
CREATE TABLE `auth_group`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of auth_group
-- ----------------------------

-- ----------------------------
-- Table structure for auth_group_permissions
-- ----------------------------
DROP TABLE IF EXISTS `auth_group_permissions`;
CREATE TABLE `auth_group_permissions`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `auth_group_permissions_group_id_permission_id_0cd325b0_uniq`(`group_id`, `permission_id`) USING BTREE,
  INDEX `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm`(`permission_id`) USING BTREE,
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of auth_group_permissions
-- ----------------------------

-- ----------------------------
-- Table structure for auth_permission
-- ----------------------------
DROP TABLE IF EXISTS `auth_permission`;
CREATE TABLE `auth_permission`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `auth_permission_content_type_id_codename_01ab375a_uniq`(`content_type_id`, `codename`) USING BTREE,
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 121 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of auth_permission
-- ----------------------------
INSERT INTO `auth_permission` VALUES (1, 'Can add permission', 1, 'add_permission');
INSERT INTO `auth_permission` VALUES (2, 'Can change permission', 1, 'change_permission');
INSERT INTO `auth_permission` VALUES (3, 'Can delete permission', 1, 'delete_permission');
INSERT INTO `auth_permission` VALUES (4, 'Can view permission', 1, 'view_permission');
INSERT INTO `auth_permission` VALUES (5, 'Can add group', 2, 'add_group');
INSERT INTO `auth_permission` VALUES (6, 'Can change group', 2, 'change_group');
INSERT INTO `auth_permission` VALUES (7, 'Can delete group', 2, 'delete_group');
INSERT INTO `auth_permission` VALUES (8, 'Can view group', 2, 'view_group');
INSERT INTO `auth_permission` VALUES (9, 'Can add user', 3, 'add_user');
INSERT INTO `auth_permission` VALUES (10, 'Can change user', 3, 'change_user');
INSERT INTO `auth_permission` VALUES (11, 'Can delete user', 3, 'delete_user');
INSERT INTO `auth_permission` VALUES (12, 'Can view user', 3, 'view_user');
INSERT INTO `auth_permission` VALUES (13, 'Can add content type', 4, 'add_contenttype');
INSERT INTO `auth_permission` VALUES (14, 'Can change content type', 4, 'change_contenttype');
INSERT INTO `auth_permission` VALUES (15, 'Can delete content type', 4, 'delete_contenttype');
INSERT INTO `auth_permission` VALUES (16, 'Can view content type', 4, 'view_contenttype');
INSERT INTO `auth_permission` VALUES (17, 'Can add session', 5, 'add_session');
INSERT INTO `auth_permission` VALUES (18, 'Can change session', 5, 'change_session');
INSERT INTO `auth_permission` VALUES (19, 'Can delete session', 5, 'delete_session');
INSERT INTO `auth_permission` VALUES (20, 'Can view session', 5, 'view_session');
INSERT INTO `auth_permission` VALUES (21, 'Can add crontab', 6, 'add_crontabschedule');
INSERT INTO `auth_permission` VALUES (22, 'Can change crontab', 6, 'change_crontabschedule');
INSERT INTO `auth_permission` VALUES (23, 'Can delete crontab', 6, 'delete_crontabschedule');
INSERT INTO `auth_permission` VALUES (24, 'Can view crontab', 6, 'view_crontabschedule');
INSERT INTO `auth_permission` VALUES (25, 'Can add interval', 7, 'add_intervalschedule');
INSERT INTO `auth_permission` VALUES (26, 'Can change interval', 7, 'change_intervalschedule');
INSERT INTO `auth_permission` VALUES (27, 'Can delete interval', 7, 'delete_intervalschedule');
INSERT INTO `auth_permission` VALUES (28, 'Can view interval', 7, 'view_intervalschedule');
INSERT INTO `auth_permission` VALUES (29, 'Can add periodic task', 8, 'add_periodictask');
INSERT INTO `auth_permission` VALUES (30, 'Can change periodic task', 8, 'change_periodictask');
INSERT INTO `auth_permission` VALUES (31, 'Can delete periodic task', 8, 'delete_periodictask');
INSERT INTO `auth_permission` VALUES (32, 'Can view periodic task', 8, 'view_periodictask');
INSERT INTO `auth_permission` VALUES (33, 'Can add periodic tasks', 9, 'add_periodictasks');
INSERT INTO `auth_permission` VALUES (34, 'Can change periodic tasks', 9, 'change_periodictasks');
INSERT INTO `auth_permission` VALUES (35, 'Can delete periodic tasks', 9, 'delete_periodictasks');
INSERT INTO `auth_permission` VALUES (36, 'Can view periodic tasks', 9, 'view_periodictasks');
INSERT INTO `auth_permission` VALUES (37, 'Can add task state', 10, 'add_taskmeta');
INSERT INTO `auth_permission` VALUES (38, 'Can change task state', 10, 'change_taskmeta');
INSERT INTO `auth_permission` VALUES (39, 'Can delete task state', 10, 'delete_taskmeta');
INSERT INTO `auth_permission` VALUES (40, 'Can view task state', 10, 'view_taskmeta');
INSERT INTO `auth_permission` VALUES (41, 'Can add saved group result', 11, 'add_tasksetmeta');
INSERT INTO `auth_permission` VALUES (42, 'Can change saved group result', 11, 'change_tasksetmeta');
INSERT INTO `auth_permission` VALUES (43, 'Can delete saved group result', 11, 'delete_tasksetmeta');
INSERT INTO `auth_permission` VALUES (44, 'Can view saved group result', 11, 'view_tasksetmeta');
INSERT INTO `auth_permission` VALUES (45, 'Can add task', 12, 'add_taskstate');
INSERT INTO `auth_permission` VALUES (46, 'Can change task', 12, 'change_taskstate');
INSERT INTO `auth_permission` VALUES (47, 'Can delete task', 12, 'delete_taskstate');
INSERT INTO `auth_permission` VALUES (48, 'Can view task', 12, 'view_taskstate');
INSERT INTO `auth_permission` VALUES (49, 'Can add worker', 13, 'add_workerstate');
INSERT INTO `auth_permission` VALUES (50, 'Can change worker', 13, 'change_workerstate');
INSERT INTO `auth_permission` VALUES (51, 'Can delete worker', 13, 'delete_workerstate');
INSERT INTO `auth_permission` VALUES (52, 'Can view worker', 13, 'view_workerstate');
INSERT INTO `auth_permission` VALUES (53, 'Can add dict', 14, 'add_dict');
INSERT INTO `auth_permission` VALUES (54, 'Can change dict', 14, 'change_dict');
INSERT INTO `auth_permission` VALUES (55, 'Can delete dict', 14, 'delete_dict');
INSERT INTO `auth_permission` VALUES (56, 'Can view dict', 14, 'view_dict');
INSERT INTO `auth_permission` VALUES (57, 'Can add permission', 15, 'add_permission');
INSERT INTO `auth_permission` VALUES (58, 'Can change permission', 15, 'change_permission');
INSERT INTO `auth_permission` VALUES (59, 'Can delete permission', 15, 'delete_permission');
INSERT INTO `auth_permission` VALUES (60, 'Can view permission', 15, 'view_permission');
INSERT INTO `auth_permission` VALUES (61, 'Can add role', 16, 'add_role');
INSERT INTO `auth_permission` VALUES (62, 'Can change role', 16, 'change_role');
INSERT INTO `auth_permission` VALUES (63, 'Can delete role', 16, 'delete_role');
INSERT INTO `auth_permission` VALUES (64, 'Can view role', 16, 'view_role');
INSERT INTO `auth_permission` VALUES (65, 'Can add user', 17, 'add_user');
INSERT INTO `auth_permission` VALUES (66, 'Can change user', 17, 'change_user');
INSERT INTO `auth_permission` VALUES (67, 'Can delete user', 17, 'delete_user');
INSERT INTO `auth_permission` VALUES (68, 'Can view user', 17, 'view_user');
INSERT INTO `auth_permission` VALUES (69, 'Can add login account', 18, 'add_loginaccount');
INSERT INTO `auth_permission` VALUES (70, 'Can change login account', 18, 'change_loginaccount');
INSERT INTO `auth_permission` VALUES (71, 'Can delete login account', 18, 'delete_loginaccount');
INSERT INTO `auth_permission` VALUES (72, 'Can view login account', 18, 'view_loginaccount');
INSERT INTO `auth_permission` VALUES (73, 'Can add asset', 19, 'add_asset');
INSERT INTO `auth_permission` VALUES (74, 'Can change asset', 19, 'change_asset');
INSERT INTO `auth_permission` VALUES (75, 'Can delete asset', 19, 'delete_asset');
INSERT INTO `auth_permission` VALUES (76, 'Can view asset', 19, 'view_asset');
INSERT INTO `auth_permission` VALUES (77, 'Can add asset group', 20, 'add_assetgroup');
INSERT INTO `auth_permission` VALUES (78, 'Can change asset group', 20, 'change_assetgroup');
INSERT INTO `auth_permission` VALUES (79, 'Can delete asset group', 20, 'delete_assetgroup');
INSERT INTO `auth_permission` VALUES (80, 'Can view asset group', 20, 'view_assetgroup');
INSERT INTO `auth_permission` VALUES (81, 'Can add idc', 21, 'add_idc');
INSERT INTO `auth_permission` VALUES (82, 'Can change idc', 21, 'change_idc');
INSERT INTO `auth_permission` VALUES (83, 'Can delete idc', 21, 'delete_idc');
INSERT INTO `auth_permission` VALUES (84, 'Can view idc', 21, 'view_idc');
INSERT INTO `auth_permission` VALUES (85, 'Can add nic', 22, 'add_nic');
INSERT INTO `auth_permission` VALUES (86, 'Can change nic', 22, 'change_nic');
INSERT INTO `auth_permission` VALUES (87, 'Can delete nic', 22, 'delete_nic');
INSERT INTO `auth_permission` VALUES (88, 'Can view nic', 22, 'view_nic');
INSERT INTO `auth_permission` VALUES (89, 'Can add mounts', 23, 'add_mounts');
INSERT INTO `auth_permission` VALUES (90, 'Can change mounts', 23, 'change_mounts');
INSERT INTO `auth_permission` VALUES (91, 'Can delete mounts', 23, 'delete_mounts');
INSERT INTO `auth_permission` VALUES (92, 'Can view mounts', 23, 'view_mounts');
INSERT INTO `auth_permission` VALUES (93, 'Can add devices', 24, 'add_devices');
INSERT INTO `auth_permission` VALUES (94, 'Can change devices', 24, 'change_devices');
INSERT INTO `auth_permission` VALUES (95, 'Can delete devices', 24, 'delete_devices');
INSERT INTO `auth_permission` VALUES (96, 'Can view devices', 24, 'view_devices');
INSERT INTO `auth_permission` VALUES (97, 'Can add asset os info', 25, 'add_assetosinfo');
INSERT INTO `auth_permission` VALUES (98, 'Can change asset os info', 25, 'change_assetosinfo');
INSERT INTO `auth_permission` VALUES (99, 'Can delete asset os info', 25, 'delete_assetosinfo');
INSERT INTO `auth_permission` VALUES (100, 'Can view asset os info', 25, 'view_assetosinfo');
INSERT INTO `auth_permission` VALUES (101, 'Can add game log', 26, 'add_gamelog');
INSERT INTO `auth_permission` VALUES (102, 'Can change game log', 26, 'change_gamelog');
INSERT INTO `auth_permission` VALUES (103, 'Can delete game log', 26, 'delete_gamelog');
INSERT INTO `auth_permission` VALUES (104, 'Can view game log', 26, 'view_gamelog');
INSERT INTO `auth_permission` VALUES (105, 'Can add tasks', 27, 'add_tasks');
INSERT INTO `auth_permission` VALUES (106, 'Can change tasks', 27, 'change_tasks');
INSERT INTO `auth_permission` VALUES (107, 'Can delete tasks', 27, 'delete_tasks');
INSERT INTO `auth_permission` VALUES (108, 'Can view tasks', 27, 'view_tasks');
INSERT INTO `auth_permission` VALUES (109, 'Can add user log', 28, 'add_userlog');
INSERT INTO `auth_permission` VALUES (110, 'Can change user log', 28, 'change_userlog');
INSERT INTO `auth_permission` VALUES (111, 'Can delete user log', 28, 'delete_userlog');
INSERT INTO `auth_permission` VALUES (112, 'Can view user log', 28, 'view_userlog');
INSERT INTO `auth_permission` VALUES (113, 'Can add game task', 29, 'add_gametask');
INSERT INTO `auth_permission` VALUES (114, 'Can change game task', 29, 'change_gametask');
INSERT INTO `auth_permission` VALUES (115, 'Can delete game task', 29, 'delete_gametask');
INSERT INTO `auth_permission` VALUES (116, 'Can view game task', 29, 'view_gametask');
INSERT INTO `auth_permission` VALUES (117, 'Can add record', 30, 'add_record');
INSERT INTO `auth_permission` VALUES (118, 'Can change record', 30, 'change_record');
INSERT INTO `auth_permission` VALUES (119, 'Can delete record', 30, 'delete_record');
INSERT INTO `auth_permission` VALUES (120, 'Can view record', 30, 'view_record');

-- ----------------------------
-- Table structure for auth_user
-- ----------------------------
DROP TABLE IF EXISTS `auth_user`;
CREATE TABLE `auth_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime(6) NULL DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(254) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of auth_user
-- ----------------------------

-- ----------------------------
-- Table structure for auth_user_groups
-- ----------------------------
DROP TABLE IF EXISTS `auth_user_groups`;
CREATE TABLE `auth_user_groups`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `auth_user_groups_user_id_group_id_94350c0c_uniq`(`user_id`, `group_id`) USING BTREE,
  INDEX `auth_user_groups_group_id_97559544_fk_auth_group_id`(`group_id`) USING BTREE,
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of auth_user_groups
-- ----------------------------

-- ----------------------------
-- Table structure for auth_user_user_permissions
-- ----------------------------
DROP TABLE IF EXISTS `auth_user_user_permissions`;
CREATE TABLE `auth_user_user_permissions`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq`(`user_id`, `permission_id`) USING BTREE,
  INDEX `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm`(`permission_id`) USING BTREE,
  CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of auth_user_user_permissions
-- ----------------------------

-- ----------------------------
-- Table structure for celery_taskmeta
-- ----------------------------
DROP TABLE IF EXISTS `celery_taskmeta`;
CREATE TABLE `celery_taskmeta`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `result` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `date_done` datetime(6) NOT NULL,
  `traceback` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `hidden` tinyint(1) NOT NULL,
  `meta` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `task_id`(`task_id`) USING BTREE,
  INDEX `celery_taskmeta_hidden_23fd02dc`(`hidden`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of celery_taskmeta
-- ----------------------------

-- ----------------------------
-- Table structure for celery_tasksetmeta
-- ----------------------------
DROP TABLE IF EXISTS `celery_tasksetmeta`;
CREATE TABLE `celery_tasksetmeta`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taskset_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `result` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `date_done` datetime(6) NOT NULL,
  `hidden` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `taskset_id`(`taskset_id`) USING BTREE,
  INDEX `celery_tasksetmeta_hidden_593cfc24`(`hidden`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of celery_tasksetmeta
-- ----------------------------

-- ----------------------------
-- Table structure for cmdb_asset
-- ----------------------------
DROP TABLE IF EXISTS `cmdb_asset`;
CREATE TABLE `cmdb_asset`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_ip` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `asset_arp` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `asset_msg` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `asset_model` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `asset_type` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `asset_status` varchar(16) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `login_port` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `login_user` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `login_id_rsa` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `login_passwd` varchar(512) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `connect_method` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `create_time` datetime(6) NOT NULL,
  `asset_group_id` int(11) NULL DEFAULT NULL,
  `idc_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `admin_ip`(`admin_ip`) USING BTREE,
  INDEX `cmdb_asset_asset_group_id_a064cee9_fk_cmdb_assetgroup_id`(`asset_group_id`) USING BTREE,
  INDEX `cmdb_asset_idc_id_51ecdaae_fk_cmdb_idc_id`(`idc_id`) USING BTREE,
  CONSTRAINT `cmdb_asset_asset_group_id_a064cee9_fk_cmdb_assetgroup_id` FOREIGN KEY (`asset_group_id`) REFERENCES `cmdb_assetgroup` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `cmdb_asset_idc_id_51ecdaae_fk_cmdb_idc_id` FOREIGN KEY (`idc_id`) REFERENCES `cmdb_idc` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cmdb_asset
-- ----------------------------
INSERT INTO `cmdb_asset` VALUES (4, '192.168.1.134', '192.168.1.134', 'testA', NULL, '服务器', 'Unknown', '22', 'root', '', 'b\'c79b2529e2c5f95e38620606d84e670f\'', 'passwd', '2020-09-27 15:18:02.406704', 1, 1);

-- ----------------------------
-- Table structure for cmdb_assetgroup
-- ----------------------------
DROP TABLE IF EXISTS `cmdb_assetgroup`;
CREATE TABLE `cmdb_assetgroup`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `group_msg` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `group_name`(`group_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cmdb_assetgroup
-- ----------------------------
INSERT INTO `cmdb_assetgroup` VALUES (1, 'test', '测试');

-- ----------------------------
-- Table structure for cmdb_assetosinfo
-- ----------------------------
DROP TABLE IF EXISTS `cmdb_assetosinfo`;
CREATE TABLE `cmdb_assetosinfo`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hostname` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `host_ip` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `os_type` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `kernel_version` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `os_version` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `product_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `cpu_model` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `cpu_core_count` varchar(16) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `mem_size` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `asset_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `cmdb_assetosinfo_asset_id_e0ba0a9d_fk_cmdb_asset_id`(`asset_id`) USING BTREE,
  CONSTRAINT `cmdb_assetosinfo_asset_id_e0ba0a9d_fk_cmdb_asset_id` FOREIGN KEY (`asset_id`) REFERENCES `cmdb_asset` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cmdb_assetosinfo
-- ----------------------------

-- ----------------------------
-- Table structure for cmdb_devices
-- ----------------------------
DROP TABLE IF EXISTS `cmdb_devices`;
CREATE TABLE `cmdb_devices`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `device_model` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `device_size` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `interface_type` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `asset_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `cmdb_devices_asset_id_18328951_fk_cmdb_asset_id`(`asset_id`) USING BTREE,
  CONSTRAINT `cmdb_devices_asset_id_18328951_fk_cmdb_asset_id` FOREIGN KEY (`asset_id`) REFERENCES `cmdb_asset` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cmdb_devices
-- ----------------------------

-- ----------------------------
-- Table structure for cmdb_idc
-- ----------------------------
DROP TABLE IF EXISTS `cmdb_idc`;
CREATE TABLE `cmdb_idc`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idc_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `idc_msg` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `idc_name`(`idc_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cmdb_idc
-- ----------------------------
INSERT INTO `cmdb_idc` VALUES (1, 'test', '测试');

-- ----------------------------
-- Table structure for cmdb_mounts
-- ----------------------------
DROP TABLE IF EXISTS `cmdb_mounts`;
CREATE TABLE `cmdb_mounts`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `mount` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `size_total` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `size_available` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `fstype` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `asset_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `cmdb_mounts_asset_id_6f970cc9_fk_cmdb_asset_id`(`asset_id`) USING BTREE,
  CONSTRAINT `cmdb_mounts_asset_id_6f970cc9_fk_cmdb_asset_id` FOREIGN KEY (`asset_id`) REFERENCES `cmdb_asset` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cmdb_mounts
-- ----------------------------

-- ----------------------------
-- Table structure for cmdb_nic
-- ----------------------------
DROP TABLE IF EXISTS `cmdb_nic`;
CREATE TABLE `cmdb_nic`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `model` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `mac` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `ip_address` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `net_mask` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `mtu` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `asset_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `cmdb_nic_asset_id_62499396_fk_cmdb_asset_id`(`asset_id`) USING BTREE,
  CONSTRAINT `cmdb_nic_asset_id_62499396_fk_cmdb_asset_id` FOREIGN KEY (`asset_id`) REFERENCES `cmdb_asset` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cmdb_nic
-- ----------------------------

-- ----------------------------
-- Table structure for django_content_type
-- ----------------------------
DROP TABLE IF EXISTS `django_content_type`;
CREATE TABLE `django_content_type`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `model` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `django_content_type_app_label_model_76bd3d3b_uniq`(`app_label`, `model`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 31 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of django_content_type
-- ----------------------------
INSERT INTO `django_content_type` VALUES (2, 'auth', 'group');
INSERT INTO `django_content_type` VALUES (1, 'auth', 'permission');
INSERT INTO `django_content_type` VALUES (3, 'auth', 'user');
INSERT INTO `django_content_type` VALUES (19, 'cmdb', 'asset');
INSERT INTO `django_content_type` VALUES (20, 'cmdb', 'assetgroup');
INSERT INTO `django_content_type` VALUES (25, 'cmdb', 'assetosinfo');
INSERT INTO `django_content_type` VALUES (24, 'cmdb', 'devices');
INSERT INTO `django_content_type` VALUES (21, 'cmdb', 'idc');
INSERT INTO `django_content_type` VALUES (23, 'cmdb', 'mounts');
INSERT INTO `django_content_type` VALUES (22, 'cmdb', 'nic');
INSERT INTO `django_content_type` VALUES (4, 'contenttypes', 'contenttype');
INSERT INTO `django_content_type` VALUES (6, 'djcelery', 'crontabschedule');
INSERT INTO `django_content_type` VALUES (7, 'djcelery', 'intervalschedule');
INSERT INTO `django_content_type` VALUES (8, 'djcelery', 'periodictask');
INSERT INTO `django_content_type` VALUES (9, 'djcelery', 'periodictasks');
INSERT INTO `django_content_type` VALUES (10, 'djcelery', 'taskmeta');
INSERT INTO `django_content_type` VALUES (11, 'djcelery', 'tasksetmeta');
INSERT INTO `django_content_type` VALUES (12, 'djcelery', 'taskstate');
INSERT INTO `django_content_type` VALUES (13, 'djcelery', 'workerstate');
INSERT INTO `django_content_type` VALUES (26, 'logs', 'gamelog');
INSERT INTO `django_content_type` VALUES (27, 'logs', 'tasks');
INSERT INTO `django_content_type` VALUES (28, 'logs', 'userlog');
INSERT INTO `django_content_type` VALUES (14, 'rbac', 'dict');
INSERT INTO `django_content_type` VALUES (18, 'rbac', 'loginaccount');
INSERT INTO `django_content_type` VALUES (15, 'rbac', 'permission');
INSERT INTO `django_content_type` VALUES (16, 'rbac', 'role');
INSERT INTO `django_content_type` VALUES (17, 'rbac', 'user');
INSERT INTO `django_content_type` VALUES (5, 'sessions', 'session');
INSERT INTO `django_content_type` VALUES (29, 'webssh', 'gametask');
INSERT INTO `django_content_type` VALUES (30, 'webssh', 'record');

-- ----------------------------
-- Table structure for django_migrations
-- ----------------------------
DROP TABLE IF EXISTS `django_migrations`;
CREATE TABLE `django_migrations`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of django_migrations
-- ----------------------------
INSERT INTO `django_migrations` VALUES (1, 'contenttypes', '0001_initial', '2020-09-27 12:10:16.607699');
INSERT INTO `django_migrations` VALUES (2, 'contenttypes', '0002_remove_content_type_name', '2020-09-27 12:10:21.214399');
INSERT INTO `django_migrations` VALUES (3, 'auth', '0001_initial', '2020-09-27 12:10:25.707346');
INSERT INTO `django_migrations` VALUES (4, 'auth', '0002_alter_permission_name_max_length', '2020-09-27 12:10:41.859122');
INSERT INTO `django_migrations` VALUES (5, 'auth', '0003_alter_user_email_max_length', '2020-09-27 12:10:43.351158');
INSERT INTO `django_migrations` VALUES (6, 'auth', '0004_alter_user_username_opts', '2020-09-27 12:10:43.467817');
INSERT INTO `django_migrations` VALUES (7, 'auth', '0005_alter_user_last_login_null', '2020-09-27 12:10:44.709493');
INSERT INTO `django_migrations` VALUES (8, 'auth', '0006_require_contenttypes_0002', '2020-09-27 12:10:44.822192');
INSERT INTO `django_migrations` VALUES (9, 'auth', '0007_alter_validators_add_error_messages', '2020-09-27 12:10:44.936886');
INSERT INTO `django_migrations` VALUES (10, 'auth', '0008_alter_user_username_max_length', '2020-09-27 12:10:46.325199');
INSERT INTO `django_migrations` VALUES (11, 'auth', '0009_alter_user_last_name_max_length', '2020-09-27 12:10:47.836126');
INSERT INTO `django_migrations` VALUES (12, 'auth', '0010_alter_group_name_max_length', '2020-09-27 12:10:49.920548');
INSERT INTO `django_migrations` VALUES (13, 'auth', '0011_update_proxy_permissions', '2020-09-27 12:10:50.127993');
INSERT INTO `django_migrations` VALUES (14, 'cmdb', '0001_initial', '2020-09-27 12:10:59.932753');
INSERT INTO `django_migrations` VALUES (15, 'djcelery', '0001_initial', '2020-09-27 12:11:14.440927');
INSERT INTO `django_migrations` VALUES (16, 'logs', '0001_initial', '2020-09-27 12:11:24.511001');
INSERT INTO `django_migrations` VALUES (17, 'rbac', '0001_initial', '2020-09-27 12:11:29.818773');
INSERT INTO `django_migrations` VALUES (18, 'sessions', '0001_initial', '2020-09-27 12:11:45.051009');
INSERT INTO `django_migrations` VALUES (19, 'webssh', '0001_initial', '2020-09-27 12:11:47.089583');
INSERT INTO `django_migrations` VALUES (20, 'webssh', '0002_record_is_connecting', '2020-10-12 11:59:14.901041');
INSERT INTO `django_migrations` VALUES (21, 'webssh', '0003_record_team_name', '2020-10-12 12:22:45.626099');

-- ----------------------------
-- Table structure for django_session
-- ----------------------------
DROP TABLE IF EXISTS `django_session`;
CREATE TABLE `django_session`  (
  `session_key` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `session_data` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`) USING BTREE,
  INDEX `django_session_expire_date_a5c62663`(`expire_date`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of django_session
-- ----------------------------
INSERT INTO `django_session` VALUES ('87yr0e9inwxfyo57slish9t7i37x5tm0', 'OThmYzk4ZTgxOWRkMWZmN2U0ZTM3MDAyMDNlZmRkNDQ1MTBmZTgzOTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiYXBwcy5yYmFjLmV4dHJhX3ZpZXdzLkN1c3RvbUJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIiLCJ1c2VybmFtZSI6ImFkbWluIiwiaXNfc3VwZXJ1c2VyIjoiMSIsInJvbGVfaWQiOjEsIm1lbnVfbGlzdCI6W3siaWQiOjIsInBlcm1zIjoiXHU4ZDQ0XHU0ZWE3XHU3YmExXHU3NDA2IiwicGVybXNfdXJsIjoiL2NtZGIvIiwicGVybXNfaW1nIjoiZmEtbGlzdCIsInR3b19tZW51IjpbeyJpZCI6MTAsInBlcm1zIjoiXHU4ZDQ0XHU0ZWE3XHU1MjE3XHU4ODY4IiwicGVybXNfdXJsIjoiL2NtZGIvYXNzZXQvIiwicGVybXNfaW1nIjoiZmEtYW5nbGUtcmlnaHQifSx7ImlkIjoxMSwicGVybXMiOiJcdTRlM2JcdTY3M2FcdTdlYzQiLCJwZXJtc191cmwiOiIvY21kYi9ncm91cC8iLCJwZXJtc19pbWciOiJmYS1hbmdsZS1yaWdodCJ9LHsiaWQiOjEyLCJwZXJtcyI6Ilx1NjczYVx1NjIzZlx1NTIxN1x1ODg2OCIsInBlcm1zX3VybCI6Ii9jbWRiL2lkYy8iLCJwZXJtc19pbWciOiJmYS1hbmdsZS1yaWdodCJ9XX0seyJpZCI6MywicGVybXMiOiJcdTY1ZTVcdTVmZDdcdTdiYTFcdTc0MDYiLCJwZXJtc191cmwiOiIvbG9nLyIsInBlcm1zX2ltZyI6ImZhLWZpbGUtdGV4dC1vIiwidHdvX21lbnUiOlt7ImlkIjo4LCJwZXJtcyI6Ilx1NzUyOFx1NjIzN1x1NjVlNVx1NWZkNyIsInBlcm1zX3VybCI6Ii9sb2cvdXNlci8iLCJwZXJtc19pbWciOiJmYS1hbmdsZS1yaWdodCJ9LHsiaWQiOjksInBlcm1zIjoiXHU3ZWM4XHU3YWVmXHU1NmRlXHU2NTNlIiwicGVybXNfdXJsIjoiL2xvZy9yZWNvcmQvIiwicGVybXNfaW1nIjoiZmEtYW5nbGUtcmlnaHQifV19LHsiaWQiOjEsInBlcm1zIjoiXHU1ZTczXHU1M2YwXHU3YmExXHU3NDA2IiwicGVybXNfdXJsIjoiL3JhYmMvIiwicGVybXNfaW1nIjoiZmEtd3JlbmNoIiwidHdvX21lbnUiOlt7ImlkIjo0LCJwZXJtcyI6Ilx1NzUyOFx1NjIzN1x1NTIxN1x1ODg2OCIsInBlcm1zX3VybCI6Ii9yYmFjL3VzZXIvIiwicGVybXNfaW1nIjoiZmEtYW5nbGUtcmlnaHQifSx7ImlkIjo1LCJwZXJtcyI6Ilx1ODlkMlx1ODI3Mlx1NTIxN1x1ODg2OCIsInBlcm1zX3VybCI6Ii9yYmFjL3JvbGUvIiwicGVybXNfaW1nIjoiZmEtYW5nbGUtcmlnaHQifSx7ImlkIjo2LCJwZXJtcyI6Ilx1Njc0M1x1OTY1MFx1NTIxN1x1ODg2OCIsInBlcm1zX3VybCI6Ii9yYmFjL3Blcm1zLyIsInBlcm1zX2ltZyI6ImZhLWFuZ2xlLXJpZ2h0In0seyJpZCI6NywicGVybXMiOiJcdTkxNGRcdTdmNmVcdTViNTdcdTUxNzgiLCJwZXJtc191cmwiOiIvcmJhYy9kaWN0LyIsInBlcm1zX2ltZyI6ImZhLWFuZ2xlLXJpZ2h0In1dfV0sInBlcm1zX2xpc3QiOlsiNjAyOWRkNDUtNDAzMS0zYWYyLWJmYWMtMWYwZmM5NjI5NGEyIiwiYWJmYTI1OGEtMTg2ZS0zNTBiLTliMzUtZWM3OGVmOTRhYTIwIiwiZGU5Y2YyNDEtMzQxMy0zODFiLWFjY2ItMjY5NzJjMmE2ODFkIiwiOTI4ZTVkMDItYjdkZS0zMzgyLTg0NDctMTM0M2U3NGZmNTU1IiwiNDQzMTAzNDgtYWY5MS0zMWVkLWI2MjctYmY0OThmMzk0ZTJjIiwiMjViYzM0MDctNmE1NS0zZWMwLWJkZmYtOTI5NmQ1YWVhNzJhIiwiODU4YzcwNmItN2ZhZC0zZjkyLWIxYWYtMTkyMTM5N2QwMDU5IiwiNTgzNDA4OTUtMWRjYy0zYzkxLWIzYjctZjBjMGM1M2FhNGQzIiwiMDBiNzBkYzAtYjdmZS0zZWJkLWFjMjktYTQyOTUwMzY3NjMyIiwiMzMzN2M0MDctYWFlZC0zMzM0LWFiZmQtNWJhOWQ2ZTE0MjZkIiwiMjk0ZTZiMTgtNzE3NC0zN2M2LTkzZmUtNjc5MDdlYzBhY2ZiIiwiMjA5MGEzOGQtMDgyOC0zM2E1LTgxMTktYzc4NTFhN2NkYjdlIiwiZWI5ZDY4ZTEtZTM5My0zNTE4LWJjMjQtNzhmYzE5MDgyNWJhIiwiMTY2MmMzMTctNDBlZC0zMWNkLWI5ZWMtMjdiMWUwMTQ4ZjFjIiwiZGQ2YjQ4ZTQtZDMyMi0zN2RkLTk2YmUtM2FkODVhNWJmOWQ1IiwiZGMxZTk3ZjMtMzg3Ni0zMmQzLThlNTctYjI3M2VjOWUwYTBhIiwiNGYxNzQ2YmQtNGRjMC0zMWI1LWJjODMtN2IwYTBkNGExYzQ5IiwiNzlmYjlhMDAtOGNhMC0zYzE3LTlhYmUtYTg0NTM4NjViMWM3IiwiZjA4NzA5MDctNTEzMi0zMTllLWJmYmUtZGU3YzVlNjRjNWQxIiwiZWFkMmI3NTMtMDAzYi0zYTdhLWFiMzMtNWM3MDA1ODNmNjNjIiwiOGRmYzgxOWMtZTgxNy0zYTk2LWE3MTgtZTE0NmNhM2FhY2UyIiwiMjk0NGM1MjQtODZiYy0zMDc3LTk5MTAtMDU4YmFkMzI2ZDA4IiwiNjA2YzdkZmMtNmNjMi0zOTE5LTkwYzgtZGJmOGEyZWI4NWE2IiwiNWQyZGU2ZTgtODUyNy0zM2Q3LWIxNzUtMjFmNTM3YTQwNjU5IiwiMGVjYmVmYTItNWU1Ny0zYWE1LTkxNWEtMmIxYzAwZjQyYjU3IiwiNGQ2NGI0NmYtZGNmYi0zNjc4LTkxNTUtZTIxYTA3N2Y1YTFmIiwiNTI5NjU2MjQtNjFmMC0zYzIxLTk3NjYtZWMwMWY2ZDdkMTBhIiwiODViNjk1NjAtODA5YS0zOTA2LThhMjQtMDY4ZTY2NWIxYmVlIiwiODViNjk1NjAtODA5YS0zOTA2LThhMjQtMDY4ZTY2NWIxYmVlIiwiN2M3Y2ZiZjktZWZjMS0zMjgzLTg5YTMtYTUwN2YxYmNiMWU4IiwiNmU2OGIzNTktNGM4NS0zZWVlLTljN2EtNWRkZjAyN2ExYjBiIiwiYTliZjMwYWYtMzBmMC0zOTBiLWIyNzUtN2MyZjEzMmEzNGIwIiwiZmQ3NGFhNzgtNzAwYi0zNmI5LWIzNzUtYWE4YmUwOThiMzMzIiwiMWE2YmFmYTUtNTlmZC0zZTVlLWEzNmEtOThkMjk3ZDkzODBiIiwiYTBkMjI2MDctZjI4ZS0zYzJiLWExZDMtNWZmMGYzYjZhNTg5IiwiZGZiN2NjMmYtMDdmZi0zY2FjLWEzZTMtZGE4YzM2YWU4MmUxIiwiMGUzNTVmYjItNDc3OS0zMzAwLWFmMDEtNDU5ZGQ4Zjc2MWU0IiwiYjhiODU1MGItNDE3OS0zMWUxLWFlNTctYmJkYzRjNGEwMTg5IiwiOGQ5ZGVjZTktODIxYi0zZjUwLTllZmQtMjFiZmNkNDQ0ZGFjIiwiYmMzOGJlY2YtOGIzOC0zODhlLWI4NTUtNzI5NzdiNGVlZGNiIiwiYjAyOGZjYWMtNmNhNC0zZjdiLThlMGMtODg0NDU3YzhjYWQyIiwiYTBmODAwYTAtZWI2ZS0zNjIzLTgxNTgtNDc3YTRmMWRlNjc3IiwiMjY5YTA2OWItN2M4Ny0zNDEwLWE5NTUtOTBmYTU4OWJkMDRmIiwiNTFkZWQ0ODYtMTg2OS0zZmM5LWFhMTYtNmM4MTkzNGViODczIiwiNmMyYzU0NjEtNWQ1MS0zNGYzLTgyM2UtMTJhZWEzYjBhOWE1IiwiODQ2MzQ2MjMtYjMwMi0zM2FjLWJlY2QtMzM3ODIyYTZhZGFmIl0sInRhc2tfY291bnQiOjAsImFibm9ybWFsX2NvdW50IjowfQ==', '2020-09-28 18:13:39.328570');
INSERT INTO `django_session` VALUES ('wys3qxk86ebdgfxmyy71w53d6hfz41j1', 'NzEwMzFhMWE2NGNjNjYxZjljZWVkMGFmYWU3ODFiMGFmOWYxMDgzMDp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiYXBwcy5yYmFjLmV4dHJhX3ZpZXdzLkN1c3RvbUJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIiLCJ1c2VybmFtZSI6ImFkbWluIiwiaXNfc3VwZXJ1c2VyIjoiMSIsInJvbGVfaWQiOjEsIm1lbnVfbGlzdCI6W3siaWQiOjIsInBlcm1zIjoiXHU4ZDQ0XHU0ZWE3XHU3YmExXHU3NDA2IiwicGVybXNfdXJsIjoiL2NtZGIvIiwicGVybXNfaW1nIjoiZmEtbGlzdCIsInR3b19tZW51IjpbeyJpZCI6MTAsInBlcm1zIjoiXHU4ZDQ0XHU0ZWE3XHU1MjE3XHU4ODY4IiwicGVybXNfdXJsIjoiL2NtZGIvYXNzZXQvIiwicGVybXNfaW1nIjoiZmEtYW5nbGUtcmlnaHQifSx7ImlkIjoxMSwicGVybXMiOiJcdTRlM2JcdTY3M2FcdTdlYzQiLCJwZXJtc191cmwiOiIvY21kYi9ncm91cC8iLCJwZXJtc19pbWciOiJmYS1hbmdsZS1yaWdodCJ9LHsiaWQiOjEyLCJwZXJtcyI6Ilx1NjczYVx1NjIzZlx1NTIxN1x1ODg2OCIsInBlcm1zX3VybCI6Ii9jbWRiL2lkYy8iLCJwZXJtc19pbWciOiJmYS1hbmdsZS1yaWdodCJ9XX0seyJpZCI6MywicGVybXMiOiJcdTY1ZTVcdTVmZDdcdTdiYTFcdTc0MDYiLCJwZXJtc191cmwiOiIvbG9nLyIsInBlcm1zX2ltZyI6ImZhLWZpbGUtdGV4dC1vIiwidHdvX21lbnUiOlt7ImlkIjo4LCJwZXJtcyI6Ilx1NzUyOFx1NjIzN1x1NjVlNVx1NWZkNyIsInBlcm1zX3VybCI6Ii9sb2cvdXNlci8iLCJwZXJtc19pbWciOiJmYS1hbmdsZS1yaWdodCJ9LHsiaWQiOjksInBlcm1zIjoiXHU3ZWM4XHU3YWVmXHU1NmRlXHU2NTNlIiwicGVybXNfdXJsIjoiL2xvZy9yZWNvcmQvIiwicGVybXNfaW1nIjoiZmEtYW5nbGUtcmlnaHQifV19LHsiaWQiOjEsInBlcm1zIjoiXHU1ZTczXHU1M2YwXHU3YmExXHU3NDA2IiwicGVybXNfdXJsIjoiL3JhYmMvIiwicGVybXNfaW1nIjoiZmEtd3JlbmNoIiwidHdvX21lbnUiOlt7ImlkIjo0LCJwZXJtcyI6Ilx1NzUyOFx1NjIzN1x1NTIxN1x1ODg2OCIsInBlcm1zX3VybCI6Ii9yYmFjL3VzZXIvIiwicGVybXNfaW1nIjoiZmEtYW5nbGUtcmlnaHQifSx7ImlkIjo1LCJwZXJtcyI6Ilx1ODlkMlx1ODI3Mlx1NTIxN1x1ODg2OCIsInBlcm1zX3VybCI6Ii9yYmFjL3JvbGUvIiwicGVybXNfaW1nIjoiZmEtYW5nbGUtcmlnaHQifSx7ImlkIjo2LCJwZXJtcyI6Ilx1Njc0M1x1OTY1MFx1NTIxN1x1ODg2OCIsInBlcm1zX3VybCI6Ii9yYmFjL3Blcm1zLyIsInBlcm1zX2ltZyI6ImZhLWFuZ2xlLXJpZ2h0In0seyJpZCI6NywicGVybXMiOiJcdTkxNGRcdTdmNmVcdTViNTdcdTUxNzgiLCJwZXJtc191cmwiOiIvcmJhYy9kaWN0LyIsInBlcm1zX2ltZyI6ImZhLWFuZ2xlLXJpZ2h0In1dfV0sInBlcm1zX2xpc3QiOlsiNjAyOWRkNDUtNDAzMS0zYWYyLWJmYWMtMWYwZmM5NjI5NGEyIiwiYWJmYTI1OGEtMTg2ZS0zNTBiLTliMzUtZWM3OGVmOTRhYTIwIiwiZGU5Y2YyNDEtMzQxMy0zODFiLWFjY2ItMjY5NzJjMmE2ODFkIiwiOTI4ZTVkMDItYjdkZS0zMzgyLTg0NDctMTM0M2U3NGZmNTU1IiwiNDQzMTAzNDgtYWY5MS0zMWVkLWI2MjctYmY0OThmMzk0ZTJjIiwiMjViYzM0MDctNmE1NS0zZWMwLWJkZmYtOTI5NmQ1YWVhNzJhIiwiODU4YzcwNmItN2ZhZC0zZjkyLWIxYWYtMTkyMTM5N2QwMDU5IiwiNTgzNDA4OTUtMWRjYy0zYzkxLWIzYjctZjBjMGM1M2FhNGQzIiwiMDBiNzBkYzAtYjdmZS0zZWJkLWFjMjktYTQyOTUwMzY3NjMyIiwiMzMzN2M0MDctYWFlZC0zMzM0LWFiZmQtNWJhOWQ2ZTE0MjZkIiwiMjk0ZTZiMTgtNzE3NC0zN2M2LTkzZmUtNjc5MDdlYzBhY2ZiIiwiMjA5MGEzOGQtMDgyOC0zM2E1LTgxMTktYzc4NTFhN2NkYjdlIiwiZWI5ZDY4ZTEtZTM5My0zNTE4LWJjMjQtNzhmYzE5MDgyNWJhIiwiMTY2MmMzMTctNDBlZC0zMWNkLWI5ZWMtMjdiMWUwMTQ4ZjFjIiwiZGQ2YjQ4ZTQtZDMyMi0zN2RkLTk2YmUtM2FkODVhNWJmOWQ1IiwiZGMxZTk3ZjMtMzg3Ni0zMmQzLThlNTctYjI3M2VjOWUwYTBhIiwiNGYxNzQ2YmQtNGRjMC0zMWI1LWJjODMtN2IwYTBkNGExYzQ5IiwiNzlmYjlhMDAtOGNhMC0zYzE3LTlhYmUtYTg0NTM4NjViMWM3IiwiZjA4NzA5MDctNTEzMi0zMTllLWJmYmUtZGU3YzVlNjRjNWQxIiwiZWFkMmI3NTMtMDAzYi0zYTdhLWFiMzMtNWM3MDA1ODNmNjNjIiwiOGRmYzgxOWMtZTgxNy0zYTk2LWE3MTgtZTE0NmNhM2FhY2UyIiwiMjk0NGM1MjQtODZiYy0zMDc3LTk5MTAtMDU4YmFkMzI2ZDA4IiwiNjA2YzdkZmMtNmNjMi0zOTE5LTkwYzgtZGJmOGEyZWI4NWE2IiwiNWQyZGU2ZTgtODUyNy0zM2Q3LWIxNzUtMjFmNTM3YTQwNjU5IiwiMGVjYmVmYTItNWU1Ny0zYWE1LTkxNWEtMmIxYzAwZjQyYjU3IiwiNGQ2NGI0NmYtZGNmYi0zNjc4LTkxNTUtZTIxYTA3N2Y1YTFmIiwiNTI5NjU2MjQtNjFmMC0zYzIxLTk3NjYtZWMwMWY2ZDdkMTBhIiwiODViNjk1NjAtODA5YS0zOTA2LThhMjQtMDY4ZTY2NWIxYmVlIiwiODViNjk1NjAtODA5YS0zOTA2LThhMjQtMDY4ZTY2NWIxYmVlIiwiN2M3Y2ZiZjktZWZjMS0zMjgzLTg5YTMtYTUwN2YxYmNiMWU4IiwiNmU2OGIzNTktNGM4NS0zZWVlLTljN2EtNWRkZjAyN2ExYjBiIiwiYTliZjMwYWYtMzBmMC0zOTBiLWIyNzUtN2MyZjEzMmEzNGIwIiwiZmQ3NGFhNzgtNzAwYi0zNmI5LWIzNzUtYWE4YmUwOThiMzMzIiwiMWE2YmFmYTUtNTlmZC0zZTVlLWEzNmEtOThkMjk3ZDkzODBiIiwiYTBkMjI2MDctZjI4ZS0zYzJiLWExZDMtNWZmMGYzYjZhNTg5IiwiZGZiN2NjMmYtMDdmZi0zY2FjLWEzZTMtZGE4YzM2YWU4MmUxIiwiMGUzNTVmYjItNDc3OS0zMzAwLWFmMDEtNDU5ZGQ4Zjc2MWU0IiwiYjhiODU1MGItNDE3OS0zMWUxLWFlNTctYmJkYzRjNGEwMTg5IiwiOGQ5ZGVjZTktODIxYi0zZjUwLTllZmQtMjFiZmNkNDQ0ZGFjIiwiYmMzOGJlY2YtOGIzOC0zODhlLWI4NTUtNzI5NzdiNGVlZGNiIiwiYjAyOGZjYWMtNmNhNC0zZjdiLThlMGMtODg0NDU3YzhjYWQyIiwiYTBmODAwYTAtZWI2ZS0zNjIzLTgxNTgtNDc3YTRmMWRlNjc3IiwiMjY5YTA2OWItN2M4Ny0zNDEwLWE5NTUtOTBmYTU4OWJkMDRmIiwiNTFkZWQ0ODYtMTg2OS0zZmM5LWFhMTYtNmM4MTkzNGViODczIiwiNmMyYzU0NjEtNWQ1MS0zNGYzLTgyM2UtMTJhZWEzYjBhOWE1Il0sInRhc2tfY291bnQiOjAsImFibm9ybWFsX2NvdW50IjowfQ==', '2020-09-28 15:44:08.462077');
INSERT INTO `django_session` VALUES ('xucq6963p4avv0zoef9is8165qfgyrcd', 'OThmYzk4ZTgxOWRkMWZmN2U0ZTM3MDAyMDNlZmRkNDQ1MTBmZTgzOTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiYXBwcy5yYmFjLmV4dHJhX3ZpZXdzLkN1c3RvbUJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIiLCJ1c2VybmFtZSI6ImFkbWluIiwiaXNfc3VwZXJ1c2VyIjoiMSIsInJvbGVfaWQiOjEsIm1lbnVfbGlzdCI6W3siaWQiOjIsInBlcm1zIjoiXHU4ZDQ0XHU0ZWE3XHU3YmExXHU3NDA2IiwicGVybXNfdXJsIjoiL2NtZGIvIiwicGVybXNfaW1nIjoiZmEtbGlzdCIsInR3b19tZW51IjpbeyJpZCI6MTAsInBlcm1zIjoiXHU4ZDQ0XHU0ZWE3XHU1MjE3XHU4ODY4IiwicGVybXNfdXJsIjoiL2NtZGIvYXNzZXQvIiwicGVybXNfaW1nIjoiZmEtYW5nbGUtcmlnaHQifSx7ImlkIjoxMSwicGVybXMiOiJcdTRlM2JcdTY3M2FcdTdlYzQiLCJwZXJtc191cmwiOiIvY21kYi9ncm91cC8iLCJwZXJtc19pbWciOiJmYS1hbmdsZS1yaWdodCJ9LHsiaWQiOjEyLCJwZXJtcyI6Ilx1NjczYVx1NjIzZlx1NTIxN1x1ODg2OCIsInBlcm1zX3VybCI6Ii9jbWRiL2lkYy8iLCJwZXJtc19pbWciOiJmYS1hbmdsZS1yaWdodCJ9XX0seyJpZCI6MywicGVybXMiOiJcdTY1ZTVcdTVmZDdcdTdiYTFcdTc0MDYiLCJwZXJtc191cmwiOiIvbG9nLyIsInBlcm1zX2ltZyI6ImZhLWZpbGUtdGV4dC1vIiwidHdvX21lbnUiOlt7ImlkIjo4LCJwZXJtcyI6Ilx1NzUyOFx1NjIzN1x1NjVlNVx1NWZkNyIsInBlcm1zX3VybCI6Ii9sb2cvdXNlci8iLCJwZXJtc19pbWciOiJmYS1hbmdsZS1yaWdodCJ9LHsiaWQiOjksInBlcm1zIjoiXHU3ZWM4XHU3YWVmXHU1NmRlXHU2NTNlIiwicGVybXNfdXJsIjoiL2xvZy9yZWNvcmQvIiwicGVybXNfaW1nIjoiZmEtYW5nbGUtcmlnaHQifV19LHsiaWQiOjEsInBlcm1zIjoiXHU1ZTczXHU1M2YwXHU3YmExXHU3NDA2IiwicGVybXNfdXJsIjoiL3JhYmMvIiwicGVybXNfaW1nIjoiZmEtd3JlbmNoIiwidHdvX21lbnUiOlt7ImlkIjo0LCJwZXJtcyI6Ilx1NzUyOFx1NjIzN1x1NTIxN1x1ODg2OCIsInBlcm1zX3VybCI6Ii9yYmFjL3VzZXIvIiwicGVybXNfaW1nIjoiZmEtYW5nbGUtcmlnaHQifSx7ImlkIjo1LCJwZXJtcyI6Ilx1ODlkMlx1ODI3Mlx1NTIxN1x1ODg2OCIsInBlcm1zX3VybCI6Ii9yYmFjL3JvbGUvIiwicGVybXNfaW1nIjoiZmEtYW5nbGUtcmlnaHQifSx7ImlkIjo2LCJwZXJtcyI6Ilx1Njc0M1x1OTY1MFx1NTIxN1x1ODg2OCIsInBlcm1zX3VybCI6Ii9yYmFjL3Blcm1zLyIsInBlcm1zX2ltZyI6ImZhLWFuZ2xlLXJpZ2h0In0seyJpZCI6NywicGVybXMiOiJcdTkxNGRcdTdmNmVcdTViNTdcdTUxNzgiLCJwZXJtc191cmwiOiIvcmJhYy9kaWN0LyIsInBlcm1zX2ltZyI6ImZhLWFuZ2xlLXJpZ2h0In1dfV0sInBlcm1zX2xpc3QiOlsiNjAyOWRkNDUtNDAzMS0zYWYyLWJmYWMtMWYwZmM5NjI5NGEyIiwiYWJmYTI1OGEtMTg2ZS0zNTBiLTliMzUtZWM3OGVmOTRhYTIwIiwiZGU5Y2YyNDEtMzQxMy0zODFiLWFjY2ItMjY5NzJjMmE2ODFkIiwiOTI4ZTVkMDItYjdkZS0zMzgyLTg0NDctMTM0M2U3NGZmNTU1IiwiNDQzMTAzNDgtYWY5MS0zMWVkLWI2MjctYmY0OThmMzk0ZTJjIiwiMjViYzM0MDctNmE1NS0zZWMwLWJkZmYtOTI5NmQ1YWVhNzJhIiwiODU4YzcwNmItN2ZhZC0zZjkyLWIxYWYtMTkyMTM5N2QwMDU5IiwiNTgzNDA4OTUtMWRjYy0zYzkxLWIzYjctZjBjMGM1M2FhNGQzIiwiMDBiNzBkYzAtYjdmZS0zZWJkLWFjMjktYTQyOTUwMzY3NjMyIiwiMzMzN2M0MDctYWFlZC0zMzM0LWFiZmQtNWJhOWQ2ZTE0MjZkIiwiMjk0ZTZiMTgtNzE3NC0zN2M2LTkzZmUtNjc5MDdlYzBhY2ZiIiwiMjA5MGEzOGQtMDgyOC0zM2E1LTgxMTktYzc4NTFhN2NkYjdlIiwiZWI5ZDY4ZTEtZTM5My0zNTE4LWJjMjQtNzhmYzE5MDgyNWJhIiwiMTY2MmMzMTctNDBlZC0zMWNkLWI5ZWMtMjdiMWUwMTQ4ZjFjIiwiZGQ2YjQ4ZTQtZDMyMi0zN2RkLTk2YmUtM2FkODVhNWJmOWQ1IiwiZGMxZTk3ZjMtMzg3Ni0zMmQzLThlNTctYjI3M2VjOWUwYTBhIiwiNGYxNzQ2YmQtNGRjMC0zMWI1LWJjODMtN2IwYTBkNGExYzQ5IiwiNzlmYjlhMDAtOGNhMC0zYzE3LTlhYmUtYTg0NTM4NjViMWM3IiwiZjA4NzA5MDctNTEzMi0zMTllLWJmYmUtZGU3YzVlNjRjNWQxIiwiZWFkMmI3NTMtMDAzYi0zYTdhLWFiMzMtNWM3MDA1ODNmNjNjIiwiOGRmYzgxOWMtZTgxNy0zYTk2LWE3MTgtZTE0NmNhM2FhY2UyIiwiMjk0NGM1MjQtODZiYy0zMDc3LTk5MTAtMDU4YmFkMzI2ZDA4IiwiNjA2YzdkZmMtNmNjMi0zOTE5LTkwYzgtZGJmOGEyZWI4NWE2IiwiNWQyZGU2ZTgtODUyNy0zM2Q3LWIxNzUtMjFmNTM3YTQwNjU5IiwiMGVjYmVmYTItNWU1Ny0zYWE1LTkxNWEtMmIxYzAwZjQyYjU3IiwiNGQ2NGI0NmYtZGNmYi0zNjc4LTkxNTUtZTIxYTA3N2Y1YTFmIiwiNTI5NjU2MjQtNjFmMC0zYzIxLTk3NjYtZWMwMWY2ZDdkMTBhIiwiODViNjk1NjAtODA5YS0zOTA2LThhMjQtMDY4ZTY2NWIxYmVlIiwiODViNjk1NjAtODA5YS0zOTA2LThhMjQtMDY4ZTY2NWIxYmVlIiwiN2M3Y2ZiZjktZWZjMS0zMjgzLTg5YTMtYTUwN2YxYmNiMWU4IiwiNmU2OGIzNTktNGM4NS0zZWVlLTljN2EtNWRkZjAyN2ExYjBiIiwiYTliZjMwYWYtMzBmMC0zOTBiLWIyNzUtN2MyZjEzMmEzNGIwIiwiZmQ3NGFhNzgtNzAwYi0zNmI5LWIzNzUtYWE4YmUwOThiMzMzIiwiMWE2YmFmYTUtNTlmZC0zZTVlLWEzNmEtOThkMjk3ZDkzODBiIiwiYTBkMjI2MDctZjI4ZS0zYzJiLWExZDMtNWZmMGYzYjZhNTg5IiwiZGZiN2NjMmYtMDdmZi0zY2FjLWEzZTMtZGE4YzM2YWU4MmUxIiwiMGUzNTVmYjItNDc3OS0zMzAwLWFmMDEtNDU5ZGQ4Zjc2MWU0IiwiYjhiODU1MGItNDE3OS0zMWUxLWFlNTctYmJkYzRjNGEwMTg5IiwiOGQ5ZGVjZTktODIxYi0zZjUwLTllZmQtMjFiZmNkNDQ0ZGFjIiwiYmMzOGJlY2YtOGIzOC0zODhlLWI4NTUtNzI5NzdiNGVlZGNiIiwiYjAyOGZjYWMtNmNhNC0zZjdiLThlMGMtODg0NDU3YzhjYWQyIiwiYTBmODAwYTAtZWI2ZS0zNjIzLTgxNTgtNDc3YTRmMWRlNjc3IiwiMjY5YTA2OWItN2M4Ny0zNDEwLWE5NTUtOTBmYTU4OWJkMDRmIiwiNTFkZWQ0ODYtMTg2OS0zZmM5LWFhMTYtNmM4MTkzNGViODczIiwiNmMyYzU0NjEtNWQ1MS0zNGYzLTgyM2UtMTJhZWEzYjBhOWE1IiwiODQ2MzQ2MjMtYjMwMi0zM2FjLWJlY2QtMzM3ODIyYTZhZGFmIl0sInRhc2tfY291bnQiOjAsImFibm9ybWFsX2NvdW50IjowfQ==', '2020-09-28 17:53:14.071949');
INSERT INTO `django_session` VALUES ('y5jzzue6dm14m37izwjswhqn64g6hzzf', 'OThmYzk4ZTgxOWRkMWZmN2U0ZTM3MDAyMDNlZmRkNDQ1MTBmZTgzOTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiYXBwcy5yYmFjLmV4dHJhX3ZpZXdzLkN1c3RvbUJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIiLCJ1c2VybmFtZSI6ImFkbWluIiwiaXNfc3VwZXJ1c2VyIjoiMSIsInJvbGVfaWQiOjEsIm1lbnVfbGlzdCI6W3siaWQiOjIsInBlcm1zIjoiXHU4ZDQ0XHU0ZWE3XHU3YmExXHU3NDA2IiwicGVybXNfdXJsIjoiL2NtZGIvIiwicGVybXNfaW1nIjoiZmEtbGlzdCIsInR3b19tZW51IjpbeyJpZCI6MTAsInBlcm1zIjoiXHU4ZDQ0XHU0ZWE3XHU1MjE3XHU4ODY4IiwicGVybXNfdXJsIjoiL2NtZGIvYXNzZXQvIiwicGVybXNfaW1nIjoiZmEtYW5nbGUtcmlnaHQifSx7ImlkIjoxMSwicGVybXMiOiJcdTRlM2JcdTY3M2FcdTdlYzQiLCJwZXJtc191cmwiOiIvY21kYi9ncm91cC8iLCJwZXJtc19pbWciOiJmYS1hbmdsZS1yaWdodCJ9LHsiaWQiOjEyLCJwZXJtcyI6Ilx1NjczYVx1NjIzZlx1NTIxN1x1ODg2OCIsInBlcm1zX3VybCI6Ii9jbWRiL2lkYy8iLCJwZXJtc19pbWciOiJmYS1hbmdsZS1yaWdodCJ9XX0seyJpZCI6MywicGVybXMiOiJcdTY1ZTVcdTVmZDdcdTdiYTFcdTc0MDYiLCJwZXJtc191cmwiOiIvbG9nLyIsInBlcm1zX2ltZyI6ImZhLWZpbGUtdGV4dC1vIiwidHdvX21lbnUiOlt7ImlkIjo4LCJwZXJtcyI6Ilx1NzUyOFx1NjIzN1x1NjVlNVx1NWZkNyIsInBlcm1zX3VybCI6Ii9sb2cvdXNlci8iLCJwZXJtc19pbWciOiJmYS1hbmdsZS1yaWdodCJ9LHsiaWQiOjksInBlcm1zIjoiXHU3ZWM4XHU3YWVmXHU1NmRlXHU2NTNlIiwicGVybXNfdXJsIjoiL2xvZy9yZWNvcmQvIiwicGVybXNfaW1nIjoiZmEtYW5nbGUtcmlnaHQifV19LHsiaWQiOjEsInBlcm1zIjoiXHU1ZTczXHU1M2YwXHU3YmExXHU3NDA2IiwicGVybXNfdXJsIjoiL3JhYmMvIiwicGVybXNfaW1nIjoiZmEtd3JlbmNoIiwidHdvX21lbnUiOlt7ImlkIjo0LCJwZXJtcyI6Ilx1NzUyOFx1NjIzN1x1NTIxN1x1ODg2OCIsInBlcm1zX3VybCI6Ii9yYmFjL3VzZXIvIiwicGVybXNfaW1nIjoiZmEtYW5nbGUtcmlnaHQifSx7ImlkIjo1LCJwZXJtcyI6Ilx1ODlkMlx1ODI3Mlx1NTIxN1x1ODg2OCIsInBlcm1zX3VybCI6Ii9yYmFjL3JvbGUvIiwicGVybXNfaW1nIjoiZmEtYW5nbGUtcmlnaHQifSx7ImlkIjo2LCJwZXJtcyI6Ilx1Njc0M1x1OTY1MFx1NTIxN1x1ODg2OCIsInBlcm1zX3VybCI6Ii9yYmFjL3Blcm1zLyIsInBlcm1zX2ltZyI6ImZhLWFuZ2xlLXJpZ2h0In0seyJpZCI6NywicGVybXMiOiJcdTkxNGRcdTdmNmVcdTViNTdcdTUxNzgiLCJwZXJtc191cmwiOiIvcmJhYy9kaWN0LyIsInBlcm1zX2ltZyI6ImZhLWFuZ2xlLXJpZ2h0In1dfV0sInBlcm1zX2xpc3QiOlsiNjAyOWRkNDUtNDAzMS0zYWYyLWJmYWMtMWYwZmM5NjI5NGEyIiwiYWJmYTI1OGEtMTg2ZS0zNTBiLTliMzUtZWM3OGVmOTRhYTIwIiwiZGU5Y2YyNDEtMzQxMy0zODFiLWFjY2ItMjY5NzJjMmE2ODFkIiwiOTI4ZTVkMDItYjdkZS0zMzgyLTg0NDctMTM0M2U3NGZmNTU1IiwiNDQzMTAzNDgtYWY5MS0zMWVkLWI2MjctYmY0OThmMzk0ZTJjIiwiMjViYzM0MDctNmE1NS0zZWMwLWJkZmYtOTI5NmQ1YWVhNzJhIiwiODU4YzcwNmItN2ZhZC0zZjkyLWIxYWYtMTkyMTM5N2QwMDU5IiwiNTgzNDA4OTUtMWRjYy0zYzkxLWIzYjctZjBjMGM1M2FhNGQzIiwiMDBiNzBkYzAtYjdmZS0zZWJkLWFjMjktYTQyOTUwMzY3NjMyIiwiMzMzN2M0MDctYWFlZC0zMzM0LWFiZmQtNWJhOWQ2ZTE0MjZkIiwiMjk0ZTZiMTgtNzE3NC0zN2M2LTkzZmUtNjc5MDdlYzBhY2ZiIiwiMjA5MGEzOGQtMDgyOC0zM2E1LTgxMTktYzc4NTFhN2NkYjdlIiwiZWI5ZDY4ZTEtZTM5My0zNTE4LWJjMjQtNzhmYzE5MDgyNWJhIiwiMTY2MmMzMTctNDBlZC0zMWNkLWI5ZWMtMjdiMWUwMTQ4ZjFjIiwiZGQ2YjQ4ZTQtZDMyMi0zN2RkLTk2YmUtM2FkODVhNWJmOWQ1IiwiZGMxZTk3ZjMtMzg3Ni0zMmQzLThlNTctYjI3M2VjOWUwYTBhIiwiNGYxNzQ2YmQtNGRjMC0zMWI1LWJjODMtN2IwYTBkNGExYzQ5IiwiNzlmYjlhMDAtOGNhMC0zYzE3LTlhYmUtYTg0NTM4NjViMWM3IiwiZjA4NzA5MDctNTEzMi0zMTllLWJmYmUtZGU3YzVlNjRjNWQxIiwiZWFkMmI3NTMtMDAzYi0zYTdhLWFiMzMtNWM3MDA1ODNmNjNjIiwiOGRmYzgxOWMtZTgxNy0zYTk2LWE3MTgtZTE0NmNhM2FhY2UyIiwiMjk0NGM1MjQtODZiYy0zMDc3LTk5MTAtMDU4YmFkMzI2ZDA4IiwiNjA2YzdkZmMtNmNjMi0zOTE5LTkwYzgtZGJmOGEyZWI4NWE2IiwiNWQyZGU2ZTgtODUyNy0zM2Q3LWIxNzUtMjFmNTM3YTQwNjU5IiwiMGVjYmVmYTItNWU1Ny0zYWE1LTkxNWEtMmIxYzAwZjQyYjU3IiwiNGQ2NGI0NmYtZGNmYi0zNjc4LTkxNTUtZTIxYTA3N2Y1YTFmIiwiNTI5NjU2MjQtNjFmMC0zYzIxLTk3NjYtZWMwMWY2ZDdkMTBhIiwiODViNjk1NjAtODA5YS0zOTA2LThhMjQtMDY4ZTY2NWIxYmVlIiwiODViNjk1NjAtODA5YS0zOTA2LThhMjQtMDY4ZTY2NWIxYmVlIiwiN2M3Y2ZiZjktZWZjMS0zMjgzLTg5YTMtYTUwN2YxYmNiMWU4IiwiNmU2OGIzNTktNGM4NS0zZWVlLTljN2EtNWRkZjAyN2ExYjBiIiwiYTliZjMwYWYtMzBmMC0zOTBiLWIyNzUtN2MyZjEzMmEzNGIwIiwiZmQ3NGFhNzgtNzAwYi0zNmI5LWIzNzUtYWE4YmUwOThiMzMzIiwiMWE2YmFmYTUtNTlmZC0zZTVlLWEzNmEtOThkMjk3ZDkzODBiIiwiYTBkMjI2MDctZjI4ZS0zYzJiLWExZDMtNWZmMGYzYjZhNTg5IiwiZGZiN2NjMmYtMDdmZi0zY2FjLWEzZTMtZGE4YzM2YWU4MmUxIiwiMGUzNTVmYjItNDc3OS0zMzAwLWFmMDEtNDU5ZGQ4Zjc2MWU0IiwiYjhiODU1MGItNDE3OS0zMWUxLWFlNTctYmJkYzRjNGEwMTg5IiwiOGQ5ZGVjZTktODIxYi0zZjUwLTllZmQtMjFiZmNkNDQ0ZGFjIiwiYmMzOGJlY2YtOGIzOC0zODhlLWI4NTUtNzI5NzdiNGVlZGNiIiwiYjAyOGZjYWMtNmNhNC0zZjdiLThlMGMtODg0NDU3YzhjYWQyIiwiYTBmODAwYTAtZWI2ZS0zNjIzLTgxNTgtNDc3YTRmMWRlNjc3IiwiMjY5YTA2OWItN2M4Ny0zNDEwLWE5NTUtOTBmYTU4OWJkMDRmIiwiNTFkZWQ0ODYtMTg2OS0zZmM5LWFhMTYtNmM4MTkzNGViODczIiwiNmMyYzU0NjEtNWQ1MS0zNGYzLTgyM2UtMTJhZWEzYjBhOWE1IiwiODQ2MzQ2MjMtYjMwMi0zM2FjLWJlY2QtMzM3ODIyYTZhZGFmIl0sInRhc2tfY291bnQiOjAsImFibm9ybWFsX2NvdW50IjowfQ==', '2020-10-13 15:22:57.009124');

-- ----------------------------
-- Table structure for djcelery_crontabschedule
-- ----------------------------
DROP TABLE IF EXISTS `djcelery_crontabschedule`;
CREATE TABLE `djcelery_crontabschedule`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `minute` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `hour` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `day_of_week` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `day_of_month` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `month_of_year` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of djcelery_crontabschedule
-- ----------------------------

-- ----------------------------
-- Table structure for djcelery_intervalschedule
-- ----------------------------
DROP TABLE IF EXISTS `djcelery_intervalschedule`;
CREATE TABLE `djcelery_intervalschedule`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `every` int(11) NOT NULL,
  `period` varchar(24) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of djcelery_intervalschedule
-- ----------------------------

-- ----------------------------
-- Table structure for djcelery_periodictask
-- ----------------------------
DROP TABLE IF EXISTS `djcelery_periodictask`;
CREATE TABLE `djcelery_periodictask`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `task` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `args` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `kwargs` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `queue` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `exchange` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `routing_key` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `expires` datetime(6) NULL DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL,
  `last_run_at` datetime(6) NULL DEFAULT NULL,
  `total_run_count` int(10) UNSIGNED NOT NULL,
  `date_changed` datetime(6) NOT NULL,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `crontab_id` int(11) NULL DEFAULT NULL,
  `interval_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name`) USING BTREE,
  INDEX `djcelery_periodictas_crontab_id_75609bab_fk_djcelery_`(`crontab_id`) USING BTREE,
  INDEX `djcelery_periodictas_interval_id_b426ab02_fk_djcelery_`(`interval_id`) USING BTREE,
  CONSTRAINT `djcelery_periodictas_crontab_id_75609bab_fk_djcelery_` FOREIGN KEY (`crontab_id`) REFERENCES `djcelery_crontabschedule` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `djcelery_periodictas_interval_id_b426ab02_fk_djcelery_` FOREIGN KEY (`interval_id`) REFERENCES `djcelery_intervalschedule` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of djcelery_periodictask
-- ----------------------------

-- ----------------------------
-- Table structure for djcelery_periodictasks
-- ----------------------------
DROP TABLE IF EXISTS `djcelery_periodictasks`;
CREATE TABLE `djcelery_periodictasks`  (
  `ident` smallint(6) NOT NULL,
  `last_update` datetime(6) NOT NULL,
  PRIMARY KEY (`ident`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of djcelery_periodictasks
-- ----------------------------

-- ----------------------------
-- Table structure for djcelery_taskstate
-- ----------------------------
DROP TABLE IF EXISTS `djcelery_taskstate`;
CREATE TABLE `djcelery_taskstate`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `task_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `tstamp` datetime(6) NOT NULL,
  `args` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `kwargs` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `eta` datetime(6) NULL DEFAULT NULL,
  `expires` datetime(6) NULL DEFAULT NULL,
  `result` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `traceback` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `runtime` double NULL DEFAULT NULL,
  `retries` int(11) NOT NULL,
  `hidden` tinyint(1) NOT NULL,
  `worker_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `task_id`(`task_id`) USING BTREE,
  INDEX `djcelery_taskstate_worker_id_f7f57a05_fk_djcelery_workerstate_id`(`worker_id`) USING BTREE,
  INDEX `djcelery_taskstate_state_53543be4`(`state`) USING BTREE,
  INDEX `djcelery_taskstate_name_8af9eded`(`name`) USING BTREE,
  INDEX `djcelery_taskstate_tstamp_4c3f93a1`(`tstamp`) USING BTREE,
  INDEX `djcelery_taskstate_hidden_c3905e57`(`hidden`) USING BTREE,
  CONSTRAINT `djcelery_taskstate_worker_id_f7f57a05_fk_djcelery_workerstate_id` FOREIGN KEY (`worker_id`) REFERENCES `djcelery_workerstate` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of djcelery_taskstate
-- ----------------------------

-- ----------------------------
-- Table structure for djcelery_workerstate
-- ----------------------------
DROP TABLE IF EXISTS `djcelery_workerstate`;
CREATE TABLE `djcelery_workerstate`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hostname` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `last_heartbeat` datetime(6) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `hostname`(`hostname`) USING BTREE,
  INDEX `djcelery_workerstate_last_heartbeat_4539b544`(`last_heartbeat`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of djcelery_workerstate
-- ----------------------------

-- ----------------------------
-- Table structure for logs_gamelog
-- ----------------------------
DROP TABLE IF EXISTS `logs_gamelog`;
CREATE TABLE `logs_gamelog`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `platform` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `action` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `user` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `req_data` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `status` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `create_time` datetime(6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of logs_gamelog
-- ----------------------------

-- ----------------------------
-- Table structure for logs_tasks
-- ----------------------------
DROP TABLE IF EXISTS `logs_tasks`;
CREATE TABLE `logs_tasks`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `task_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `task_result` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `exec_user` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `status` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `create_time` datetime(6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of logs_tasks
-- ----------------------------

-- ----------------------------
-- Table structure for logs_userlog
-- ----------------------------
DROP TABLE IF EXISTS `logs_userlog`;
CREATE TABLE `logs_userlog`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `action` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `menu` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `req_data` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `create_time` datetime(6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of logs_userlog
-- ----------------------------
INSERT INTO `logs_userlog` VALUES (1, 'admin', '添加资产', '资产列表', 'SUCCESS', '{\'admin_ip\': \'192.168.1.134\', \'login_port\': \'22\', \'login_user\': \'root\', \'login_passwd\': \'lzx@2019\', \'login_id_rsa\': \'\', \'connect_method\': \'passwd\', \'buy_time\': \'\', \'use_cycle\': \'1\', \'asset_group\': \'\', \'asset_idc\': \'\', \'asset_msg\': \'testA\'}', '2020-09-27 15:15:19.238457');
INSERT INTO `logs_userlog` VALUES (2, 'admin', '添加组', '主机组', 'SUCCESS', '{\'group_name\': \'test\', \'group_msg\': \'测试\'}', '2020-09-27 15:18:19.023232');
INSERT INTO `logs_userlog` VALUES (3, 'admin', '添加机房', '机房列表', 'SUCCESS', '{\'idc_name\': \'test\', \'idc_msg\': \'测试\'}', '2020-09-27 15:18:28.649470');
INSERT INTO `logs_userlog` VALUES (4, 'admin', '初始化连接', '资产列表', 'SUCCESS', '{\'asset_id\': \'4\'}', '2020-09-27 15:18:46.404717');
INSERT INTO `logs_userlog` VALUES (5, 'admin', '初始化连接', '资产列表', 'SUCCESS', '{\'asset_id\': \'4\'}', '2020-09-27 15:19:16.144154');
INSERT INTO `logs_userlog` VALUES (6, 'admin', '获取角色权限', '角色列表', 'SUCCESS', '{\'role_id\': \'1\'}', '2020-09-27 17:48:52.161388');
INSERT INTO `logs_userlog` VALUES (7, 'admin', '修改角色权限', '角色列表', 'SUCCESS', '{\'node_id_json\': \'[1,\"1#4\",\"1#4#23\",\"1#4#24\",\"1#4#25\",\"1#4#28\",\"1#4#46\",\"1#5\",\"1#5#26\",\"1#5#27\",\"1#5#29\",\"1#5#30\",\"1#5#31\",\"1#5#32\",\"1#5#33\",\"1#6\",\"1#6#34\",\"1#6#35\",\"1#6#36\",\"1#7\",\"1#7#37\",\"1#7#38\",\"1#7#39\",2,\"2#10\",\"2#10#13\",\"2#10#14\",\"2#10#15\",\"2#10#40\",\"2#10#41\",\"2#10#42\",\"2#10#43\",\"2#10#44\",\"2#10#45\",\"2#11\",\"2#11#16\",\"2#11#17\",\"2#11#18\",\"2#12\",\"2#12#19\",\"2#12#20\",\"2#12#21\",3,\"3#8\",\"3#8#22\",\"3#9\"]\', \'role_id\': \'1\'}', '2020-09-27 17:48:58.392712');
INSERT INTO `logs_userlog` VALUES (8, 'admin', '修改密码', '用户列表', 'SUCCESS', '{\'new_passwd\': \'admin\', \'user_id\': \'1\'}', '2020-09-27 17:49:26.200295');
INSERT INTO `logs_userlog` VALUES (9, 'admin', '修改资产', '资产列表', 'SUCCESS', '{\"asset_id\":\"4\"}', '2020-10-11 11:31:23.559986');
INSERT INTO `logs_userlog` VALUES (10, 'admin', '修改资产', '资产列表', 'SUCCESS', '{\"asset_id\":\"4\"}', '2020-10-11 11:43:35.509587');
INSERT INTO `logs_userlog` VALUES (11, 'admin', '修改资产', '资产列表', 'SUCCESS', '{\"asset_id\":\"4\"}', '2020-10-11 11:43:43.493563');

-- ----------------------------
-- Table structure for rbac_dict
-- ----------------------------
DROP TABLE IF EXISTS `rbac_dict`;
CREATE TABLE `rbac_dict`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dict_key` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `dict_val` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `dict_msg` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `dict_key`(`dict_key`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of rbac_dict
-- ----------------------------
INSERT INTO `rbac_dict` VALUES (1, 'ENCRYPT_KEY', '94jfdajopfjd[ajg', '加密字符串');

-- ----------------------------
-- Table structure for rbac_loginaccount
-- ----------------------------
DROP TABLE IF EXISTS `rbac_loginaccount`;
CREATE TABLE `rbac_loginaccount`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login_user` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `login_passwd` varchar(512) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `login_id_rsa` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `id_rsa_pwd` varchar(512) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `rbac_loginaccount_user_id_b06d0a55_fk_rbac_user_id`(`user_id`) USING BTREE,
  CONSTRAINT `rbac_loginaccount_user_id_b06d0a55_fk_rbac_user_id` FOREIGN KEY (`user_id`) REFERENCES `rbac_user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of rbac_loginaccount
-- ----------------------------
INSERT INTO `rbac_loginaccount` VALUES (1, 'root', 'b\'c79b2529e2c5f95e38620606d84e670f\'', '', '', 1);

-- ----------------------------
-- Table structure for rbac_permission
-- ----------------------------
DROP TABLE IF EXISTS `rbac_permission`;
CREATE TABLE `rbac_permission`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `perms_type` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `perms_method` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `perms_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `perms_icon` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `weight` int(11) NULL DEFAULT NULL,
  `perms_status` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `rbac_permission_parent_id_bcb411ef_fk_rbac_permission_id`(`parent_id`) USING BTREE,
  CONSTRAINT `rbac_permission_parent_id_bcb411ef_fk_rbac_permission_id` FOREIGN KEY (`parent_id`) REFERENCES `rbac_permission` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 47 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of rbac_permission
-- ----------------------------
INSERT INTO `rbac_permission` VALUES (1, '/rabc/', '平台管理', '一级菜单', 'GET', '6029dd45-4031-3af2-bfac-1f0fc96294a2', 'fa-wrench', 1999, 'ON', NULL);
INSERT INTO `rbac_permission` VALUES (2, '/cmdb/', '资产管理', '一级菜单', 'GET', 'abfa258a-186e-350b-9b35-ec78ef94aa20', 'fa-list', 1, 'ON', NULL);
INSERT INTO `rbac_permission` VALUES (3, '/log/', '日志管理', '一级菜单', 'GET', 'de9cf241-3413-381b-accb-26972c2a681d', 'fa-file-text-o', 1800, 'ON', NULL);
INSERT INTO `rbac_permission` VALUES (4, '/rbac/user/', '用户列表', '二级菜单', 'GET', '928e5d02-b7de-3382-8447-1343e74ff555', 'fa-angle-right', NULL, 'ON', 1);
INSERT INTO `rbac_permission` VALUES (5, '/rbac/role/', '角色列表', '二级菜单', 'GET', '44310348-af91-31ed-b627-bf498f394e2c', 'fa-angle-right', NULL, 'ON', 1);
INSERT INTO `rbac_permission` VALUES (6, '/rbac/perms/', '权限列表', '二级菜单', 'GET', '25bc3407-6a55-3ec0-bdff-9296d5aea72a', 'fa-angle-right', NULL, 'ON', 1);
INSERT INTO `rbac_permission` VALUES (7, '/rbac/dict/', '配置字典', '二级菜单', 'GET', '858c706b-7fad-3f92-b1af-1921397d0059', 'fa-angle-right', NULL, 'ON', 1);
INSERT INTO `rbac_permission` VALUES (8, '/log/user/', '用户日志', '二级菜单', 'GET', '58340895-1dcc-3c91-b3b7-f0c0c53aa4d3', 'fa-angle-right', NULL, 'ON', 3);
INSERT INTO `rbac_permission` VALUES (9, '/log/record/', '终端回放', '二级菜单', 'GET', '00b70dc0-b7fe-3ebd-ac29-a42950367632', 'fa-angle-right', NULL, 'ON', 3);
INSERT INTO `rbac_permission` VALUES (10, '/cmdb/asset/', '资产列表', '二级菜单', 'GET', '3337c407-aaed-3334-abfd-5ba9d6e1426d', 'fa-angle-right', NULL, 'ON', 2);
INSERT INTO `rbac_permission` VALUES (11, '/cmdb/group/', '主机组', '二级菜单', 'GET', '294e6b18-7174-37c6-93fe-67907ec0acfb', 'fa-angle-right', NULL, 'ON', 2);
INSERT INTO `rbac_permission` VALUES (12, '/cmdb/idc/', '机房列表', '二级菜单', 'GET', '2090a38d-0828-33a5-8119-c7851a7cdb7e', 'fa-angle-right', NULL, 'ON', 2);
INSERT INTO `rbac_permission` VALUES (13, '/cmdb/asset/', '添加资产', '功能按钮', 'POST', 'eb9d68e1-e393-3518-bc24-78fc190825ba', '', NULL, 'ON', 10);
INSERT INTO `rbac_permission` VALUES (14, '/cmdb/asset/', '修改资产', '功能按钮', 'PUT', '1662c317-40ed-31cd-b9ec-27b1e0148f1c', '', NULL, 'ON', 10);
INSERT INTO `rbac_permission` VALUES (15, '/cmdb/asset/', '删除资产', '功能按钮', 'DELETE', 'dd6b48e4-d322-37dd-96be-3ad85a5bf9d5', '', NULL, 'ON', 10);
INSERT INTO `rbac_permission` VALUES (16, '/cmdb/group/', '添加组', '功能按钮', 'POST', 'dc1e97f3-3876-32d3-8e57-b273ec9e0a0a', '', NULL, 'ON', 11);
INSERT INTO `rbac_permission` VALUES (17, '/cmdb/group/', '修改组', '功能按钮', 'PUT', '4f1746bd-4dc0-31b5-bc83-7b0a0d4a1c49', '', NULL, 'ON', 11);
INSERT INTO `rbac_permission` VALUES (18, '/cmdb/group/', '删除组', '功能按钮', 'DELETE', '79fb9a00-8ca0-3c17-9abe-a8453865b1c7', '', NULL, 'ON', 11);
INSERT INTO `rbac_permission` VALUES (19, '/cmdb/idc/', '添加机房', '功能按钮', 'POST', 'f0870907-5132-319e-bfbe-de7c5e64c5d1', '', NULL, 'ON', 12);
INSERT INTO `rbac_permission` VALUES (20, '/cmdb/idc/', '修改机房', '功能按钮', 'PUT', 'ead2b753-003b-3a7a-ab33-5c700583f63c', '', NULL, 'ON', 12);
INSERT INTO `rbac_permission` VALUES (21, '/cmdb/idc/', '删除机房', '功能按钮', 'DELETE', '8dfc819c-e817-3a96-a718-e146ca3aace2', '', NULL, 'ON', 12);
INSERT INTO `rbac_permission` VALUES (22, '/log/ulgsearch/', '日志搜索', '功能按钮', 'POST', '2944c524-86bc-3077-9910-058bad326d08', '', NULL, 'ON', 8);
INSERT INTO `rbac_permission` VALUES (23, '/rbac/user/', '添加用户', '功能按钮', 'POST', '606c7dfc-6cc2-3919-90c8-dbf8a2eb85a6', '', NULL, 'ON', 4);
INSERT INTO `rbac_permission` VALUES (24, '/rbac/user/', '修改用户', '功能按钮', 'PUT', '5d2de6e8-8527-33d7-b175-21f537a40659', '', NULL, 'ON', 4);
INSERT INTO `rbac_permission` VALUES (25, '/rbac/user/', '删除用户', '功能按钮', 'DELETE', '0ecbefa2-5e57-3aa5-915a-2b1c00f42b57', '', NULL, 'ON', 4);
INSERT INTO `rbac_permission` VALUES (26, '/rbac/role/', '添加角色', '功能按钮', 'POST', '4d64b46f-dcfb-3678-9155-e21a077f5a1f', '', NULL, 'ON', 5);
INSERT INTO `rbac_permission` VALUES (27, '/rbac/role/', '修改角色', '功能按钮', 'PUT', '52965624-61f0-3c21-9766-ec01f6d7d10a', '', NULL, 'ON', 5);
INSERT INTO `rbac_permission` VALUES (28, '/rbac/role/', '删除角色', '功能按钮', 'DELETE', '85b69560-809a-3906-8a24-068e665b1bee', '', NULL, 'ON', 4);
INSERT INTO `rbac_permission` VALUES (29, '/rbac/role/', '删除角色', '功能按钮', 'DELETE', '85b69560-809a-3906-8a24-068e665b1bee', '', NULL, 'ON', 5);
INSERT INTO `rbac_permission` VALUES (30, '/rbac/getperms/', '获取角色权限', '功能按钮', 'POST', '7c7cfbf9-efc1-3283-89a3-a507f1bcb1e8', '', NULL, 'ON', 5);
INSERT INTO `rbac_permission` VALUES (31, '/rbac/addperms/', '修改角色权限', '功能按钮', 'POST', '6e68b359-4c85-3eee-9c7a-5ddf027a1b0b', '', NULL, 'ON', 5);
INSERT INTO `rbac_permission` VALUES (32, '/rbac/getasset/', '获取资产权限', '功能按钮', 'POST', 'a9bf30af-30f0-390b-b275-7c2f132a34b0', '', NULL, 'ON', 5);
INSERT INTO `rbac_permission` VALUES (33, '/rbac/addasset/', '修改资产权限', '功能按钮', 'POST', 'fd74aa78-700b-36b9-b375-aa8be098b333', '', NULL, 'ON', 5);
INSERT INTO `rbac_permission` VALUES (34, '/rbac/perms/', '添加权限', '功能按钮', 'POST', '1a6bafa5-59fd-3e5e-a36a-98d297d9380b', '', NULL, 'ON', 6);
INSERT INTO `rbac_permission` VALUES (35, '/rbac/perms/', '修改权限', '功能按钮', 'PUT', 'a0d22607-f28e-3c2b-a1d3-5ff0f3b6a589', '', NULL, 'ON', 6);
INSERT INTO `rbac_permission` VALUES (36, '/rbac/perms/', '删除权限', '功能按钮', 'DELETE', 'dfb7cc2f-07ff-3cac-a3e3-da8c36ae82e1', '', NULL, 'ON', 6);
INSERT INTO `rbac_permission` VALUES (37, '/rbac/dict/', '添加字典', '功能按钮', 'POST', '0e355fb2-4779-3300-af01-459dd8f761e4', '', NULL, 'ON', 7);
INSERT INTO `rbac_permission` VALUES (38, '/rbac/dict/', '修改字典', '功能按钮', 'PUT', 'b8b8550b-4179-31e1-ae57-bbdc4c4a0189', '', NULL, 'ON', 7);
INSERT INTO `rbac_permission` VALUES (39, '/rbac/dict/', '删除字典', '功能按钮', 'DELETE', '8d9dece9-821b-3f50-9efd-21bfcd444dac', '', NULL, 'ON', 7);
INSERT INTO `rbac_permission` VALUES (40, '/cmdb/setconf/', '初始化连接', '功能按钮', 'POST', 'bc38becf-8b38-388e-b855-72977b4eedcb', '', NULL, 'ON', 10);
INSERT INTO `rbac_permission` VALUES (41, '/cmdb/assetdetail/', '资产详情', '功能按钮', 'GET', 'b028fcac-6ca4-3f7b-8e0c-884457c8cad2', '', NULL, 'ON', 10);
INSERT INTO `rbac_permission` VALUES (42, '/cmdb/search/', '资产过滤', '功能按钮', 'POST', 'a0f800a0-eb6e-3623-8158-477a4f1de677', '', NULL, 'ON', 10);
INSERT INTO `rbac_permission` VALUES (43, '/cmdb/uploadasset/', '资产导入', '功能按钮', 'POST', '269a069b-7c87-3410-a955-90fa589bd04f', '', NULL, 'ON', 10);
INSERT INTO `rbac_permission` VALUES (44, '/cmdb/exporthost/', '资产导出', '功能按钮', 'POST', '51ded486-1869-3fc9-aa16-6c81934eb873', '', NULL, 'ON', 10);
INSERT INTO `rbac_permission` VALUES (45, '/cmdb/recycling/', '资产回收', '功能按钮', 'POST', '6c2c5461-5d51-34f3-823e-12aea3b0a9a5', '', NULL, 'ON', 10);
INSERT INTO `rbac_permission` VALUES (46, '/rbac/chpasswd/', '修改密码', '功能按钮', 'POST', '84634623-b302-33ac-becd-337822a6adaf', '', NULL, 'ON', 4);

-- ----------------------------
-- Table structure for rbac_role
-- ----------------------------
DROP TABLE IF EXISTS `rbac_role`;
CREATE TABLE `rbac_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of rbac_role
-- ----------------------------
INSERT INTO `rbac_role` VALUES (1, '管理员');

-- ----------------------------
-- Table structure for rbac_role_asset
-- ----------------------------
DROP TABLE IF EXISTS `rbac_role_asset`;
CREATE TABLE `rbac_role_asset`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `asset_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `rbac_role_asset_role_id_asset_id_c81e7bc8_uniq`(`role_id`, `asset_id`) USING BTREE,
  INDEX `rbac_role_asset_asset_id_2a6e9b91_fk_cmdb_asset_id`(`asset_id`) USING BTREE,
  CONSTRAINT `rbac_role_asset_asset_id_2a6e9b91_fk_cmdb_asset_id` FOREIGN KEY (`asset_id`) REFERENCES `cmdb_asset` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `rbac_role_asset_role_id_74d924a4_fk_rbac_role_id` FOREIGN KEY (`role_id`) REFERENCES `rbac_role` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of rbac_role_asset
-- ----------------------------

-- ----------------------------
-- Table structure for rbac_role_permissions
-- ----------------------------
DROP TABLE IF EXISTS `rbac_role_permissions`;
CREATE TABLE `rbac_role_permissions`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `rbac_role_permissions_role_id_permission_id_d01303da_uniq`(`role_id`, `permission_id`) USING BTREE,
  INDEX `rbac_role_permission_permission_id_f5e1e866_fk_rbac_perm`(`permission_id`) USING BTREE,
  CONSTRAINT `rbac_role_permission_permission_id_f5e1e866_fk_rbac_perm` FOREIGN KEY (`permission_id`) REFERENCES `rbac_permission` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `rbac_role_permissions_role_id_d10416cb_fk_rbac_role_id` FOREIGN KEY (`role_id`) REFERENCES `rbac_role` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 104 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of rbac_role_permissions
-- ----------------------------
INSERT INTO `rbac_role_permissions` VALUES (58, 1, 1);
INSERT INTO `rbac_role_permissions` VALUES (81, 1, 2);
INSERT INTO `rbac_role_permissions` VALUES (100, 1, 3);
INSERT INTO `rbac_role_permissions` VALUES (59, 1, 4);
INSERT INTO `rbac_role_permissions` VALUES (65, 1, 5);
INSERT INTO `rbac_role_permissions` VALUES (73, 1, 6);
INSERT INTO `rbac_role_permissions` VALUES (77, 1, 7);
INSERT INTO `rbac_role_permissions` VALUES (101, 1, 8);
INSERT INTO `rbac_role_permissions` VALUES (103, 1, 9);
INSERT INTO `rbac_role_permissions` VALUES (82, 1, 10);
INSERT INTO `rbac_role_permissions` VALUES (92, 1, 11);
INSERT INTO `rbac_role_permissions` VALUES (96, 1, 12);
INSERT INTO `rbac_role_permissions` VALUES (83, 1, 13);
INSERT INTO `rbac_role_permissions` VALUES (84, 1, 14);
INSERT INTO `rbac_role_permissions` VALUES (85, 1, 15);
INSERT INTO `rbac_role_permissions` VALUES (93, 1, 16);
INSERT INTO `rbac_role_permissions` VALUES (94, 1, 17);
INSERT INTO `rbac_role_permissions` VALUES (95, 1, 18);
INSERT INTO `rbac_role_permissions` VALUES (97, 1, 19);
INSERT INTO `rbac_role_permissions` VALUES (98, 1, 20);
INSERT INTO `rbac_role_permissions` VALUES (99, 1, 21);
INSERT INTO `rbac_role_permissions` VALUES (102, 1, 22);
INSERT INTO `rbac_role_permissions` VALUES (60, 1, 23);
INSERT INTO `rbac_role_permissions` VALUES (61, 1, 24);
INSERT INTO `rbac_role_permissions` VALUES (62, 1, 25);
INSERT INTO `rbac_role_permissions` VALUES (66, 1, 26);
INSERT INTO `rbac_role_permissions` VALUES (67, 1, 27);
INSERT INTO `rbac_role_permissions` VALUES (63, 1, 28);
INSERT INTO `rbac_role_permissions` VALUES (68, 1, 29);
INSERT INTO `rbac_role_permissions` VALUES (69, 1, 30);
INSERT INTO `rbac_role_permissions` VALUES (70, 1, 31);
INSERT INTO `rbac_role_permissions` VALUES (71, 1, 32);
INSERT INTO `rbac_role_permissions` VALUES (72, 1, 33);
INSERT INTO `rbac_role_permissions` VALUES (74, 1, 34);
INSERT INTO `rbac_role_permissions` VALUES (75, 1, 35);
INSERT INTO `rbac_role_permissions` VALUES (76, 1, 36);
INSERT INTO `rbac_role_permissions` VALUES (78, 1, 37);
INSERT INTO `rbac_role_permissions` VALUES (79, 1, 38);
INSERT INTO `rbac_role_permissions` VALUES (80, 1, 39);
INSERT INTO `rbac_role_permissions` VALUES (86, 1, 40);
INSERT INTO `rbac_role_permissions` VALUES (87, 1, 41);
INSERT INTO `rbac_role_permissions` VALUES (88, 1, 42);
INSERT INTO `rbac_role_permissions` VALUES (89, 1, 43);
INSERT INTO `rbac_role_permissions` VALUES (90, 1, 44);
INSERT INTO `rbac_role_permissions` VALUES (91, 1, 45);
INSERT INTO `rbac_role_permissions` VALUES (64, 1, 46);

-- ----------------------------
-- Table structure for rbac_user
-- ----------------------------
DROP TABLE IF EXISTS `rbac_user`;
CREATE TABLE `rbac_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `nick_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `date_create` datetime(6) NOT NULL,
  `is_active` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `is_superuser` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime(6) NOT NULL,
  `is_online` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `role_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `rbac_user_role_id_864cf958_fk_rbac_role_id`(`role_id`) USING BTREE,
  CONSTRAINT `rbac_user_role_id_864cf958_fk_rbac_role_id` FOREIGN KEY (`role_id`) REFERENCES `rbac_role` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of rbac_user
-- ----------------------------
INSERT INTO `rbac_user` VALUES (1, 'admin', '小贰', 'b\'031e767fa17a41175854455d409aa060\'', '2019-07-17 10:17:33.829924', '0', '1', '2020-10-11 10:30:16.000665', 'online', 1);

-- ----------------------------
-- Table structure for webssh_gametask
-- ----------------------------
DROP TABLE IF EXISTS `webssh_gametask`;
CREATE TABLE `webssh_gametask`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_name` varchar(1024) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `platform_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `game_sid` varchar(512) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `frontend_platform_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `frontend_num` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `create_date` datetime(6) NOT NULL,
  `status` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `maintain_time` datetime(6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of webssh_gametask
-- ----------------------------

-- ----------------------------
-- Table structure for webssh_record
-- ----------------------------
DROP TABLE IF EXISTS `webssh_record`;
CREATE TABLE `webssh_record`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(6) NOT NULL,
  `host` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `user` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `filename` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `end_time` datetime(6) NOT NULL,
  `is_connecting` tinyint(1) NOT NULL,
  `team_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 46 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of webssh_record
-- ----------------------------
INSERT INTO `webssh_record` VALUES (34, '2020-10-12 14:32:17.968208', '192.168.1.134', 'admin', '4_admin_1602484337.cast', '2020-10-12 14:32:24.333486', 0, 'wssh_4_admin_1602484337');
INSERT INTO `webssh_record` VALUES (35, '2020-10-12 14:48:16.247080', '192.168.1.134', 'admin', '4_admin_1602485296.cast', '2020-10-12 15:13:46.407023', 0, 'wssh_4_admin_1602485296');
INSERT INTO `webssh_record` VALUES (36, '2020-10-12 14:57:54.108432', '192.168.1.134', 'admin', '4_admin_1602485873.cast', '2020-10-12 14:58:13.605236', 0, 'wssh_4_admin_1602485873');
INSERT INTO `webssh_record` VALUES (37, '2020-10-12 14:58:13.758006', '192.168.1.134', 'admin', '4_admin_1602485893.cast', '2020-10-12 15:13:45.397216', 0, 'wssh_4_admin_1602485893');
INSERT INTO `webssh_record` VALUES (38, '2020-10-12 15:03:27.674212', '192.168.1.134', 'admin', '4_admin_1602486207.cast', '2020-10-12 15:13:44.637427', 0, 'wssh_4_admin_1602486207');
INSERT INTO `webssh_record` VALUES (39, '2020-10-12 15:04:24.730004', '192.168.1.134', 'admin', '4_admin_1602486264.cast', '2020-10-12 15:13:43.671569', 0, 'wssh_4_admin_1602486264');
INSERT INTO `webssh_record` VALUES (40, '2020-10-12 15:05:29.660025', '192.168.1.134', 'admin', '4_admin_1602486329.cast', '2020-10-12 15:13:42.357852', 0, 'wssh_4_admin_1602486329');
INSERT INTO `webssh_record` VALUES (41, '2020-10-12 15:06:00.664311', '192.168.1.134', 'admin', '4_admin_1602486360.cast', '2020-10-12 15:06:11.327858', 0, 'wssh_4_admin_1602486360');
INSERT INTO `webssh_record` VALUES (42, '2020-10-12 15:06:11.489511', '192.168.1.134', 'admin', '4_admin_1602486371.cast', '2020-10-12 15:06:58.485873', 0, 'wssh_4_admin_1602486371');
INSERT INTO `webssh_record` VALUES (43, '2020-10-12 15:14:01.993667', '192.168.1.134', 'admin', '4_admin_1602486841.cast', '2020-10-12 15:14:10.478039', 0, 'wssh_4_admin_1602486841');
INSERT INTO `webssh_record` VALUES (44, '2020-10-12 15:18:32.677572', '192.168.1.134', 'admin', '4_admin_1602487112.cast', '2020-10-12 15:19:00.517059', 0, 'wssh_4_admin_1602487112');
INSERT INTO `webssh_record` VALUES (45, '2020-10-12 15:19:06.955052', '192.168.1.134', 'admin', '4_admin_1602487146.cast', '2020-10-12 15:19:18.916928', 0, 'wssh_4_admin_1602487146');

SET FOREIGN_KEY_CHECKS = 1;
