import json
import uuid
import datetime
from django.shortcuts import render,HttpResponse
from django.views import View
from apps.logs import models as db_log
from apps.webssh import models as ws_db
from apps.rbac import models as rbac_db
from django.core.paginator import PageNotAnInteger,Paginator,EmptyPage
from plugins.public import page as pg
from django.utils.decorators import method_decorator
from apps.rbac.extra_views import login_check,perms_check
from django.db.models import Q




class UserLogView(View):
    """用户操作记录"""
    @method_decorator(login_check)
    @method_decorator(perms_check)
    def dispatch(self, request, *args, **kwargs):
        return super(UserLogView,self).dispatch(request,*args, **kwargs)

    def get(self,request,page=1):
        caption = "用户日志"

        if request.session['is_superuser'] == "1":
            db_obj = db_log.UserLog.objects.all().order_by("-create_time")[:50]
        else:
            username = request.session["username"]
            db_obj = db_log.UserLog.objects.filter(user=username).order_by("-create_time")[:50]

        pagesize = 15
        paginator = Paginator(db_obj,pagesize)
        currentPage = int(page)
        page_nums = paginator.num_pages
        page_list = pg.control(currentPage, page_nums)
        try:
            data_list = paginator.page(page)  # 获取当前页码的记录
        except PageNotAnInteger:
            data_list = paginator.page(1)  # 如果用户输入的页码不是整数时,显示第1页的内容
        except EmptyPage:
            data_list = paginator.page(paginator.num_pages)
        return render(request, 'logs/userlog_list.html', locals())

@login_check
def search_userlog(request):
    """查询任务"""
    search_key = request.POST.get('search_key', None)
    data_list = []
    if search_key:
        if request.session['is_superuser'] == "1":
            data_list = db_log.UserLog.objects.filter(Q(user__icontains=search_key) |(Q(menu__icontains=search_key) | Q(create_time__icontains=search_key)| Q(req_data__icontains=search_key))).order_by("-create_time")
        else:
            username = request.session["username"]
            data_list = db_log.UserLog.objects.filter(Q(user=username),Q(user__icontains=search_key) |(Q(menu__icontains=search_key) | Q(create_time__icontains=search_key)| Q(req_data__icontains=search_key))).order_by("-create_time")

    return render(request, "logs/userlog_search.html", locals())




class RecordView(View):
    """终端回放"""
    @method_decorator(login_check)
    @method_decorator(perms_check)
    def dispatch(self, request, *args, **kwargs):
        return super(RecordView,self).dispatch(request,*args, **kwargs)

    def get(self,request,page=1):
        caption = "终端回放"
        if request.session['is_superuser'] == "1":
            db_obj = ws_db.Record.objects.all().order_by("-create_time")[:50]
        else:
            username = request.session["username"]
            nick_name = rbac_db.User.objects.get(username=username).username
            db_obj = ws_db.Record.objects.filter(user=nick_name).order_by("-create_time")[:50]

        pagesize = 11
        paginator = Paginator(db_obj,pagesize)
        currentPage = int(page)
        page_nums = paginator.num_pages
        page_list = pg.control(currentPage, page_nums)
        try:
            data_list = paginator.page(page)  # 获取当前页码的记录
        except PageNotAnInteger:
            data_list = paginator.page(1)  # 如果用户输入的页码不是整数时,显示第1页的内容
        except EmptyPage:
            data_list = paginator.page(paginator.num_pages)
        return render(request, 'logs/webssh_record.html', locals())


@login_check
def search_record(request):
    """查询webssh操作记录"""
    search_key = request.POST.get('search_key', None)
    data_list = []
    if search_key:
        if request.session['is_superuser'] == "1":
            data_list = ws_db.Record.objects.filter(Q(user__icontains=search_key) |(Q(host__icontains=search_key) | Q(create_time__icontains=search_key))).order_by("-create_time")
        else:
            username = request.session["username"]
            nick_name = rbac_db.User.objects.get(username=username).username
            data_list = db_log.UserLog.objects.filter(Q(user=username),Q(user__icontains=search_key) |(Q(host__icontains=search_key) | Q(create_time__icontains=search_key))).order_by("-create_time")
    return render(request, "logs/record_search.html", locals())




