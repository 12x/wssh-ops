from wsshOps.celery import app
from multiprocessing import current_process
from apps.logs import  models as db_log
from celery.result import AsyncResult


@app.task
def auto_sync_tasks():
    current_process()._config = {'semprefix': '/mp'}
    task_obj = db_log.Tasks.objects.filter(status="PENDING")
    for i in task_obj:
        async = AsyncResult(id=i.task_id, app=app)
        status = async.state
        if status == 'SUCCESS':
            result = async.get()
            i.task_result = result
        elif status == 'FAILURE':
            result = async.traceback
            i.task_result = result
        else:
            result = "任务执行中，请耐心等待..."
            i.task_result = result
        i.status = status
        i.save()
    return "sync tasks status success"





