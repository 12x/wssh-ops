"""wsshOps URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from  apps.logs import views
from django.urls import path

urlpatterns = [
    path("user/",views.UserLogView.as_view()),
    path("user/<int:page>/", views.UserLogView.as_view()),
    path("ulgsearch/", views.search_userlog),
    path("record/",views.RecordView.as_view()),
    path("record/<int:page>/", views.RecordView.as_view()),
    path("rdsearch/", views.search_record),
]
