from django.db import models
# Create your models here.


class UserLog(models.Model):
    user = models.CharField(max_length=32)
    action = models.CharField(max_length=64)
    menu = models.CharField(max_length=64)
    status = models.CharField(max_length=64)
    req_data = models.TextField(null=True)
    create_time = models.DateTimeField(auto_now_add=True)
    def __unicode__(self):
        return self.user
