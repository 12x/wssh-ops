import datetime
from django.db import models


# Create your models here.

class Asset(models.Model):
    """资产表"""
    admin_ip = models.CharField(max_length=64, unique=True)
    asset_arp = models.CharField(max_length=128, null=True)
    asset_msg = models.CharField(max_length=256, null=True)
    asset_model = models.CharField(max_length=128, null=True)
    asset_type = models.CharField(default='服务器', max_length=128, null=True)
    asset_status = models.CharField(default="Unknown", max_length=16)
    login_port = models.CharField(max_length=64)
    login_user = models.CharField(max_length=64)
    login_id_rsa = models.TextField(null=True)
    login_passwd = models.CharField(max_length=512)
    connect_method = models.CharField(max_length=64)
    create_time = models.DateTimeField(auto_now_add=True)
    asset_group = models.ForeignKey(to="AssetGroup", on_delete=models.SET_NULL, null=True)
    idc = models.ForeignKey(to="IDC", on_delete=models.SET_NULL, null=True)


class AssetOsInfo(models.Model):
    asset = models.ForeignKey(to="Asset", on_delete=models.CASCADE)
    hostname = models.CharField(max_length=128, null=True)
    host_ip = models.CharField(max_length=128, null=True)
    os_type = models.CharField(max_length=128, null=True)
    kernel_version = models.CharField(max_length=128, null=True)
    os_version = models.CharField(max_length=128, null=True)
    product_name = models.CharField(max_length=128, null=True)
    cpu_model = models.CharField('CPU型号', max_length=128, blank=True, null=True)
    cpu_core_count = models.CharField(max_length=16, default=1)
    mem_size = models.CharField(max_length=128, null=True)


class Devices(models.Model):
    """存储设备"""
    asset = models.ForeignKey('Asset', on_delete=models.CASCADE)
    device_name = models.CharField(max_length=128)
    device_model = models.CharField(max_length=128, null=True)
    device_size = models.CharField(max_length=128, null=True)
    interface_type = models.CharField(max_length=128, default='unknown')


class Mounts(models.Model):
    asset = models.ForeignKey('Asset', on_delete=models.CASCADE)
    device = models.CharField(max_length=128)
    mount = models.CharField(max_length=128, null=True)
    size_total = models.CharField(max_length=128, null=True)
    size_available = models.CharField(max_length=128, null=True)
    fstype = models.CharField(max_length=64, null=True)


class NIC(models.Model):
    """网卡组件"""
    asset = models.ForeignKey('Asset', on_delete=models.CASCADE)
    name = models.CharField(max_length=64)
    model = models.CharField(max_length=64, null=True)
    mac = models.CharField(max_length=128, null=True)
    ip_address = models.CharField(max_length=64, null=True)
    net_mask = models.CharField(max_length=64, null=True)
    mtu = models.CharField(max_length=64, null=True)


class AssetGroup(models.Model):
    """资产分组表"""
    group_name = models.CharField(max_length=64, unique=True)
    group_msg = models.CharField(max_length=256, null=True)

    def __unicode__(self):
        return self.group_name


class IDC(models.Model):
    """机房管理信息"""
    idc_name = models.CharField(max_length=64, unique=True)
    idc_msg = models.CharField(max_length=128, null=True)

    def __unicode__(self):
        return self.idc_name
