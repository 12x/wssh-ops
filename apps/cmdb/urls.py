"""wsshOps URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from apps.cmdb import views, extra_views
from django.urls import path

urlpatterns = [
    path("asset/", views.AssetView.as_view()),
    path("asset/<int:page>/", views.AssetView.as_view()),
    path("getassetip/<str:ip>/", views.GetAssetIp),
    path("group/", views.GroupView.as_view()),
    path("group/<int:page>/", views.GroupView.as_view()),
    path("idc/", views.IdcView.as_view()),
    path("idc/<int:page>/", views.IdcView.as_view()),
    path("setconf/", extra_views.set_connect),
    path("recycling/", extra_views.asset_recycling),
    path("assetdetail/<int:id>/", extra_views.asset_detail),
    path("search/", extra_views.search_asset),
    path("uploadasset/", extra_views.upload_asset),
    path("exporthost/", extra_views.export_host),
    path("zabbix/", extra_views.zabbix),
]
