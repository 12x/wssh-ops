from django.db import models
from apps.cmdb.models import Asset


class Permission(models.Model):
    """
    权限表
    不可以做菜单的权限    menu=null
    """
    url = models.CharField(max_length=32, verbose_name='权限')
    title = models.CharField(max_length=32, verbose_name='标题')
    perms_type = models.CharField(max_length=32, verbose_name='权限类型', default="权限")
    perms_method = models.CharField(max_length=32, verbose_name='请求类型')
    perms_id = models.CharField(max_length=64, verbose_name='权限id', null=True)
    perms_icon = models.CharField(max_length=32, verbose_name='图标', null=True)
    weight = models.IntegerField(verbose_name='菜单排序', null=True)
    parent = models.ForeignKey(to='Permission', on_delete=models.CASCADE, null=True)
    perms_status = models.CharField(max_length=32, verbose_name='权限状态', default="ON")
    def __str__(self):
        return self.title


class Role(models.Model):
    """
    角色表
    """
    title = models.CharField(max_length=32, verbose_name='名称')
    permissions = models.ManyToManyField(to='Permission', verbose_name='角色权限', blank=True)
    asset = models.ManyToManyField(to=Asset, verbose_name='角色资产', blank=True)

    def __str__(self):
        return self.title


class User(models.Model):
    """
    用户表
    """
    username = models.CharField(max_length=32, verbose_name='名称')
    nick_name = models.CharField(max_length=32, verbose_name='昵称')
    password = models.CharField(max_length=128, verbose_name='密码')
    date_create = models.DateTimeField(auto_now_add=True, verbose_name='创建时间')
    is_active = models.CharField(max_length=128, verbose_name='是否激活', default=0)
    is_superuser = models.CharField(max_length=32, verbose_name='是否超级管理员', default=False)
    last_login = models.DateTimeField(auto_now=True)
    is_online = models.CharField(max_length=32, verbose_name='是否在线', default="offline")
    role = models.ForeignKey(to='Role', on_delete=models.SET_NULL, verbose_name='用户角色', null=True)

    def __str__(self):
        return self.username


class LoginAccount(models.Model):
    user = models.ForeignKey(to=User, on_delete=models.CASCADE)
    login_user = models.CharField(max_length=64, null=False)
    login_passwd = models.CharField(max_length=512, null=True)
    login_id_rsa = models.TextField(null=True)
    id_rsa_pwd = models.CharField(max_length=512, null=True)


class Dict(models.Model):
    """
    配置字典表
    """
    dict_key = models.CharField(max_length=32, verbose_name='键', unique=True)
    dict_val = models.TextField(verbose_name='值')
    dict_msg = models.CharField(max_length=128, verbose_name='备注', null=True)

    def __str__(self):
        return self.dict_key
