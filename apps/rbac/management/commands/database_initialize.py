from django.core.management import BaseCommand
from apps.rbac import models as db_rabc
from plugins.public import encryption

class Command(BaseCommand):
    def handle(self, *args, **options):
        dict_key = "ENCRYPT_KEY"
        dict_val = r"94jfdajopfjd[ajg"
        dict_msg = "加密字符串"
        dict_obj = db_rabc.Dict(dict_key=dict_key, dict_val=dict_val, dict_msg=dict_msg)
        dict_obj.save()

        #添加管理员
        role_obj = db_rabc.Role(title="管理员")
        role_obj.save()

        username = "admin"
        nick_name = "小贰"
        password = "admin"
        is_superuser = "1"
        role_id = role_obj.id
        str_key = db_rabc.Dict.objects.get(dict_key="ENCRYPT_KEY").dict_val
        # 加密密码
        pc = encryption.prpcrypt(str_key)
        en_password = pc.encrypt(password)
        user_obj = db_rabc.User(username=username, nick_name=nick_name, password=en_password, role_id=role_id,is_superuser=is_superuser)
        user_obj.save()