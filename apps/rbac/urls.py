"""wsshOps URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from  apps.rbac import views,extra_views
from django.urls import path

urlpatterns = [
    path("user/",views.UserView.as_view()),
    path("user/<int:page>/", views.UserView.as_view()),
    path("role/",views.RoleView.as_view()),
    path("role/<int:page>/", views.RoleView.as_view()),
    path("perms/",views.PermsView.as_view()),
    path("perms/<int:page>/", views.PermsView.as_view()),
    path("dict/",views.DictView.as_view()),
    path("dict/<int:page>/", views.DictView.as_view()),
    path("chpasswd/", extra_views.change_passwd),
    path("addlguser/", extra_views.add_lg_user),
    path("getperms/", extra_views.get_role_perms),
    path("addperms/", extra_views.add_role_perms),
    path("getasset/", extra_views.get_role_asset),
    path("addasset/", extra_views.add_role_asset),
    path("getplatform/", extra_views.get_role_platform),
    path("addplatform/", extra_views.add_role_platform),
]
