from django.urls import path
from apps.webssh import consumers

websocket_urlpatterns = [
    path('webssh/', consumers.WebsshConsumer),
]