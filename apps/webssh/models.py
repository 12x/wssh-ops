from django.db import models
import django.utils.timezone as timezone
import time
#Create your models here.



class Record(models.Model):
    create_time = models.DateTimeField(auto_now_add=True, verbose_name='创建时间')
    host = models.CharField(max_length=128,null=True, verbose_name='主机')
    user = models.CharField(max_length=128,null=True, verbose_name='用户')
    filename = models.CharField(max_length=128, verbose_name='录像文件名称')
    end_time = models.DateTimeField(verbose_name='结束时间',auto_now=True)
    is_connecting = models.BooleanField(default=True)
    team_name =  models.CharField(max_length=128,null=True)
    def __str__(self):
        return self.host

